<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200127190623 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE quiz_candidat_response ADD quiz_candidat_answer_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE quiz_candidat_response ADD CONSTRAINT FK_F6E900E82E459BD2 FOREIGN KEY (quiz_candidat_answer_id) REFERENCES quiz_candidat_answer (id)');
        $this->addSql('CREATE INDEX IDX_F6E900E82E459BD2 ON quiz_candidat_response (quiz_candidat_answer_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE quiz_candidat_response DROP FOREIGN KEY FK_F6E900E82E459BD2');
        $this->addSql('DROP INDEX IDX_F6E900E82E459BD2 ON quiz_candidat_response');
        $this->addSql('ALTER TABLE quiz_candidat_response DROP quiz_candidat_answer_id');
    }
}
