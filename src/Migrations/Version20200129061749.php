<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200129061749 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE hard_skills_candidat (id INT AUTO_INCREMENT NOT NULL, candidat_id INT NOT NULL, hard_skill_id INT NOT NULL, date DATETIME NOT NULL, score INT NOT NULL, INDEX IDX_1EB0ADE78D0EB82 (candidat_id), INDEX IDX_1EB0ADE7B7DB062 (hard_skill_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE quiz_canidat_response (id INT AUTO_INCREMENT NOT NULL, quiz_candidat_answer_id INT DEFAULT NULL, INDEX IDX_669EE2B42E459BD2 (quiz_candidat_answer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE hard_skills_candidat ADD CONSTRAINT FK_1EB0ADE78D0EB82 FOREIGN KEY (candidat_id) REFERENCES koann_user (id)');
        $this->addSql('ALTER TABLE hard_skills_candidat ADD CONSTRAINT FK_1EB0ADE7B7DB062 FOREIGN KEY (hard_skill_id) REFERENCES hard_skills (id)');
        $this->addSql('ALTER TABLE quiz_canidat_response ADD CONSTRAINT FK_669EE2B42E459BD2 FOREIGN KEY (quiz_candidat_answer_id) REFERENCES quiz_candidat_answer (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE hard_skills_candidat');
        $this->addSql('DROP TABLE quiz_canidat_response');
    }
}
