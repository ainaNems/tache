<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200211141136 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE quiz_candidate_answer (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE quiz_canidat_response (id INT AUTO_INCREMENT NOT NULL, quiz_candidat_answer_id INT DEFAULT NULL, INDEX IDX_669EE2B42E459BD2 (quiz_candidat_answer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE quiz_canidat_response ADD CONSTRAINT FK_669EE2B42E459BD2 FOREIGN KEY (quiz_candidat_answer_id) REFERENCES quiz_candidat_answer (id)');
        $this->addSql('ALTER TABLE koann_user ADD telephone VARCHAR(15) DEFAULT NULL, ADD instagram VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE quiz_candidate_answer');
        $this->addSql('DROP TABLE quiz_canidat_response');
        $this->addSql('ALTER TABLE koann_user DROP telephone, DROP instagram');
    }
}
