<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200213160511 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE domain CHANGE domain_code domain_code INT DEFAULT NULL, CHANGE domain_concept_type domain_concept_type VARCHAR(20) DEFAULT NULL, CHANGE domain_desc domain_desc LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE sector CHANGE code code INT DEFAULT NULL, CHANGE description description LONGTEXT DEFAULT NULL, CHANGE concept_type concept_type VARCHAR(20) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE domain CHANGE domain_code domain_code INT NOT NULL, CHANGE domain_concept_type domain_concept_type VARCHAR(20) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE domain_desc domain_desc LONGTEXT NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE sector CHANGE code code INT NOT NULL, CHANGE description description LONGTEXT NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE concept_type concept_type VARCHAR(20) NOT NULL COLLATE utf8mb4_unicode_ci');
    }
}
