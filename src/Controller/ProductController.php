<?php

namespace App\Controller;

use App\Entity\Produit;
use App\Form\ProduitType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    /**
     * @Route("/produit", name="produit")
     */
    public function indexAction()
    {
        $_produit   = new Produit();
        $_form      = $this->createForm(ProduitType::class,$_produit);

        return $this->render('produit/index.html.twig', [
            'form'     => $_form->createView(),
            'produit'  => $_produit
        ]);
    }
    /**
     * @param Request $_request
     * @return JsonResponse
     * @Route("/save",name="produit_save")
     */
    public function saveAction(Request $_request)
    {
        $_produit   = new Produit();
        $_produit->setDesignation($_request->request->get('designation'));
        $_produit->setQuantite($_request->request->get('quantite'));
        $_entity_manager = $this->getDoctrine()->getManager();
        $_entity_manager->persist($_produit);
        $_entity_manager->flush();

        return new JsonResponse(['message' => "ajout fait avec succès"]);
    }

    /**
     * @param Request $_request
     * @Route("/liste",name="get_all_produits", methods={"GET"})
     * @return JsonResponse
     */
    public function getAllAction(Request $_request)
    {
        $_page = $_request->query->get('page');
        $_produits          = $this->getDoctrine()->getManager()->getRepository(Produit::class)->paginateProdcut($_page,5);
        $_number_of_product = count($this->getDoctrine()->getManager()->getRepository(Produit::class)->findAll());
        $_response = array();
        foreach ($_produits as $_produit) {
            $_response[] = array(
                'id'          => $_produit->getId(),
                'designation' => $_produit->getDesignation(),
                'quantite'    => $_produit->getQuantite(),
            );
        }

        return new JsonResponse(['produits' => $_response,'number' => $_number_of_product]);
    }
    /**
     * @param Request $_request
     * @Route("/liste/page",name="get_page_produits", methods={"GET"})
     * @return JsonResponse

    public function getProductPageAction(Request $_request)
    {
        $_page = $_request->query->get('page');
        $_produits = $this->getDoctrine()->getManager()->getRepository(Produit::class)->paginateProdcut($_page,5);
        $_response = array();
        foreach ($_produits as $_produit) {
            $_response[] = array(
                'id'          => $_produit->getId(),
                'designation' => $_produit->getDesignation(),
                'quantite'    => $_produit->getQuantite(),
            );
        }

        return new JsonResponse(['produits' => $_response]);
    }*/
    /**
     * @param Request $_request
     * @return JsonResponse
     * @Route("/delete",name="produit_delete")
     */
    public function deleteAction(Request $_request)
    {
        $_produit_id     = $_request->request->get('id');
        $_produit        = $this->getDoctrine()->getRepository(Produit::class)->find($_produit_id);
        $_entity_manager = $this->getDoctrine()->getManager();
        $_entity_manager->remove($_produit);
        $_entity_manager->flush();

        return new JsonResponse(['message' => "Suppression fait avec succès"]);
    }
    /**
     * @param Request $_request
     * @return JsonResponse
     * @Route("/one",name="get_one_produit", methods={"GET"})
     */
    public function getOneAction(Request $_request)
    {
        $_produit_id = $_request->query->get('id');
        $_produit    = $this->getDoctrine()->getRepository(Produit::class)->find($_produit_id);
        $_response   = array(
            'id'          => $_produit->getId(),
            'designation' => $_produit->getDesignation(),
            'quantite'    => $_produit->getQuantite(),
        );

        return new JsonResponse(['produit' => $_response]);
    }
    /**
     * @param Request $_request
     * @Route("/update",name="produit_update",methods = {"POST"})
     * @return JsonResponse
     */
    public function updateAction(Request $_request)
    {
        $_produit_id = $_request->request->get('id');
        $_produit    = $this->getDoctrine()->getRepository(Produit::class)->find($_produit_id);
        $_produit->setDesignation($_request->request->get('designation'));
        $_produit->setQuantite($_request->request->get('quantite'));
        $_entity_manager = $this->getDoctrine()->getManager();
        $_entity_manager->flush();

        return new JsonResponse(['message' => 'mis à jour fait avec succes']);
    }
    /**
     * @param Request $_request
     * @Route("/filtre",name ="produit_filter")
     * @return JsonResponse
     */
    public function filterAction(Request $_request)
    {
        $_filter   = $_request->query->get('filtre');
        $_produits = $this->getDoctrine()->getRepository(Produit::class)->findByProductDessignation($_filter);
        $_response = array();
        foreach ($_produits as $_produit) {
            $_response[] = array(
                'id'          => $_produit->getId(),
                'designation' => $_produit->getDesignation(),
                'quantite'    => $_produit->getQuantite(),
            );
        }

        return new JsonResponse(['produits'=>$_response]) ;
    }
}
