<?php
/**
 * Created by PhpStorm.
 * User: Livenexx
 * Date: 29/11/2019
 * Time: 12:19
 */

namespace App\Controller;



use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LoginController extends AbstractController
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @Route("/", name="home_index", methods={"GET"})
     */
    public function index(Request $request)
    {
        return $this->render('login/login_page.html.twig', [
        ]);
    }
}