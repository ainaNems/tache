<?php

namespace App\Controller;

use App\Entity\Utilisateur;
use App\Form\UtilisateurType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
/**
 * @Route("/utilisateur")
 */
class UtilisateurController extends AbstractController
{
    /**
     * @Route("/", name="utilisateur_index", methods={"GET"})
     */
    public function indexAction()
    {
        $_utilisateurs = $this->getDoctrine()
            ->getRepository(Utilisateur::class)
            ->findAll();

        return $this->render('utilisateur/index.html.twig', [
            'utilisateurs' => $_utilisateurs,
        ]);
    }
    /**
     * @param Request $request
     * @return Response
     */
    public function newAction(Request $request)
    {
        $_user = new Utilisateur();
        $_form = $this->createForm(UtilisateurType::class, $_user);
        $_form -> handleRequest($request);
        if ($_form->isSubmitted() && $_form->isValid())
        {
            $_pic_file = $_form['pics']->getData();
            if($_pic_file)
            {
                $_new_file_name = $this->getFileName($_pic_file);
                try {
                    $_pic_file->move(
                        $this->getParameter('pics'),
                        $_new_file_name
                    );
                } catch (FileException $e) {
                }
                $_user->setPicUrl($_new_file_name);
            }
            $_entity_manager = $this->getDoctrine()->getManager();
            $_entity_manager -> persist($_user);
            $_entity_manager -> flush();

            return $this->redirectToRoute('utilisateur_index');
        }

        return $this->render('utilisateur/new.html.twig', [
            'utilisateur' => $_user,
            'form'        => $_form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="utilisateur_show", methods={"GET"})
     */
    public function showAction(Utilisateur $_utilisateur)
    {
        return $this->render('utilisateur/show.html.twig', [
            'utilisateur' => $_utilisateur,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="utilisateur_edit", methods={"GET","POST"})
     */
    public function editAction(Request $request, Utilisateur $_utilisateur)
    {
        $_form = $this->createForm(UtilisateurType::class, $_utilisateur);
        $_form -> handleRequest($request);
        if ($_form->isSubmitted() && $_form->isValid()) {
            $_pic_file=$_form['pics']->getData();
            if($_pic_file)
            {
                $_new_file_name = $this->getFileName($_pic_file);
                try {
                    $_pic_file->move(
                        $this->getParameter('pics'),
                        $_new_file_name
                    );
                } catch (FileException $e) {}
                $_utilisateur->setPicUrl($_new_file_name);
            }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('utilisateur_index');
        }

        return $this->render('utilisateur/edit.html.twig', [
            'utilisateur' => $_utilisateur,
            'form'        => $_form->createView(),
        ]);
    }
    public function getFileName($_pic_file)
    {
        $_pics_file_name = pathinfo($_pic_file->getClientOriginalName(), PATHINFO_FILENAME);
        $_safe_file_name = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $_pics_file_name);
        $_new_file_name  = $_safe_file_name.'-'.uniqid().'.'.$_pic_file->guessExtension();

        return $_new_file_name;
    }
    /**
     * @Route("/{id}", name="utilisateur_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, Utilisateur $_utilisateur)
    {
        if ($this->isCsrfTokenValid('delete'.$_utilisateur->getId(), $request->request->get('_token'))) {
            $_entity_manager = $this->getDoctrine()->getManager();
            $_entity_manager->remove($_utilisateur);
            $_entity_manager->flush();
        }

        return $this->redirectToRoute('utilisateur_index');
    }
    public function saveFileAction($_pic_file)
    {
        $_pics_file_name = pathinfo($_pic_file->getClientOriginalName(), PATHINFO_FILENAME);
        $_safe_file_name = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $_pics_file_name);
        $_new_file_name  = $_safe_file_name.'-'.uniqid().'.'.$_pic_file->guessExtension();
        try {
            $_pic_file->move(
                $this->getParameter('pics'),
                $_new_file_name
            );
            return $_new_file_name;
        } catch (FileException $e) {

            return null;
        }
    }

}
