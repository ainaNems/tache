<?php

namespace App\Controller;

use App\Entity\QuizType;
use App\Form\QuizTypeType;
use App\Repository\QuizTypeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/quiz-type")
 */
class QuizTypeController extends AbstractController
{
    /**
     * @param QuizTypeRepository $quizTypeRepository
     * @return Response
     * @Route("/", name="quiz_type_index", methods={"GET","POST"})
     */
    public function index(QuizTypeRepository $quizTypeRepository,Request $request)
    {
        $quizType = new QuizType();
        $form = $this->createForm(QuizTypeType::class, $quizType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($quizType);
            $entityManager->flush();

            return $this->redirectToRoute('quiz_type_index');
        }

        return $this->render('quiz_type/index.html.twig', [
            'quiz_types' => $quizTypeRepository->findAll(),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/new", name="quiz_type_new", methods={"GET","POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function newQuizType(Request $request)
    {
        $quizType = new QuizType();
        $form = $this->createForm(QuizTypeType::class, $quizType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($quizType);
            $entityManager->flush();

            return $this->redirectToRoute('quiz_type_index');
        }

        return $this->render('quiz_type/new.html.twig', [
            'quiz_type' => $quizType,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="quiz_type_show", methods={"GET"})
     * @param QuizType $quizType
     * @return Response
     */
    public function show(QuizType $quizType)
    {
        return $this->render('quiz_type/show.html.twig', [
            'quiz_type' => $quizType,
        ]);
    }

    /**
     * @param Request $request
     * @param QuizType $quizType
     * @return Response
     * @Route("/{id}/edit", name="quiz_type_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, QuizType $quizType)
    {
        $form = $this->createForm(QuizTypeType::class, $quizType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('quiz_type_index');
        }

        return $this->render('quiz_type/edit.html.twig', [
            'quiz_type' => $quizType,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Request $request
     * @param QuizType $quizType
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/{id}", name="quiz_type_delete", methods={"DELETE"})
     */
    public function delete(Request $request, QuizType $quizType)
    {
        if ($this->isCsrfTokenValid('delete'.$quizType->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($quizType);
            $entityManager->flush();
        }

        return $this->redirectToRoute('quiz_type_index');
    }
}
