<?php

namespace App\Controller;

use App\Entity\SsCandidat;
use App\Form\SsCandidatType;
use App\Repository\SsCandidatRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/ss/candidat")
 */
class SsCandidatController extends AbstractController
{
    /**
     * @Route("/", name="ss_candidat_index", methods={"GET"})
     * @param SsCandidatRepository $ssCandidatRepository
     * @return Response
     */
    public function index(SsCandidatRepository $ssCandidatRepository)
    {
        return $this->render('ss_candidat/index.html.twig', [
            'ss_candidats' => $ssCandidatRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="ss_candidat_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function newSsCandidat(Request $request)
    {
        $ssCandidat = new SsCandidat();
        $form = $this->createForm(SsCandidatType::class, $ssCandidat);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($ssCandidat);
            $entityManager->flush();

            return $this->redirectToRoute('ss_candidat_index');
        }

        return $this->render('ss_candidat/new.html.twig', [
            'ss_candidat' => $ssCandidat,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="ss_candidat_show", methods={"GET"})
     * @param SsCandidat $ssCandidat
     * @return Response
     */
    public function show(SsCandidat $ssCandidat)
    {
        return $this->render('ss_candidat/show.html.twig', [
            'ss_candidat' => $ssCandidat,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="ss_candidat_edit", methods={"GET","POST"})
     * @param Request $request
     * @param SsCandidat $ssCandidat
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function edit(Request $request, SsCandidat $ssCandidat)
    {
        $form = $this->createForm(SsCandidatType::class, $ssCandidat);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('ss_candidat_index');
        }

        return $this->render('ss_candidat/edit.html.twig', [
            'ss_candidat' => $ssCandidat,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="ss_candidat_delete", methods={"DELETE"})
     * @param Request $request
     * @param SsCandidat $ssCandidat
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete(Request $request, SsCandidat $ssCandidat)
    {
        if ($this->isCsrfTokenValid('delete'.$ssCandidat->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($ssCandidat);
            $entityManager->flush();
        }

        return $this->redirectToRoute('ss_candidat_index');
    }
}
