<?php

namespace App\Controller;

use App\Entity\SoftSkills;
use App\Form\SoftSkillsType;
use App\Repository\SoftSkillsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/soft/skills")
 */
class SoftSkillsController extends AbstractController
{
    /**
     * @Route("/", name="soft_skills_index", methods={"GET","POST"})
     * @param SoftSkillsRepository $softSkillsRepository
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function index(SoftSkillsRepository $softSkillsRepository,Request $request)
    {
        $softSkill = new SoftSkills();
        $form = $this->createForm(SoftSkillsType::class, $softSkill);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($softSkill);
            $entityManager->flush();

            return $this->redirectToRoute('soft_skills_index');
        }

        return $this->render('soft_skills/index.html.twig', [
            'soft_skills' => $softSkillsRepository->findAll(),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/new", name="soft_skills_new", methods={"GET","POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function newSoftSkills(Request $request)
    {
        $softSkill = new SoftSkills();
        $form = $this->createForm(SoftSkillsType::class, $softSkill);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($softSkill);
            $entityManager->flush();

            return $this->redirectToRoute('soft_skills_index');
        }

        return $this->render('soft_skills/new.html.twig', [
            'soft_skill' => $softSkill,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="soft_skills_show", methods={"GET"})
     * @param SoftSkills $softSkill
     * @return Response
     */
    public function show(SoftSkills $softSkill)
    {
        return $this->render('soft_skills/show.html.twig', [
            'soft_skill' => $softSkill,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="soft_skills_edit", methods={"GET","POST"})
     * @param Request $request
     * @param SoftSkills $softSkill
     * @return Response
     */
    public function edit(Request $request,SoftSkillsRepository $softSkillsRepository, SoftSkills $softSkill)
    {
        $form = $this->createForm(SoftSkillsType::class, $softSkill);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('soft_skills_index');
        }

        return $this->render('soft_skills/index.html.twig', [
            'soft_skill' => $softSkill,
            'form' => $form->createView(),
            'soft_skills' => $softSkillsRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="soft_skills_delete", methods={"DELETE"})
     * @param Request $request
     * @param SoftSkills $softSkill
     * @return Response
     */
    public function delete(Request $request, SoftSkills $softSkill)
    {
        if ($this->isCsrfTokenValid('delete'.$softSkill->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($softSkill);
            $entityManager->flush();
        }

        return $this->redirectToRoute('soft_skills_index');
    }
}
