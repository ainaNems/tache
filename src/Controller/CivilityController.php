<?php

namespace App\Controller;

use App\Entity\Civility;
use App\Form\CivilityType;
use App\Repository\CivilityRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/civility")
 */
class CivilityController extends AbstractController
{
    /**
     * @Route("/", name="civility_index", methods={"GET","POST"})
     */
    public function indexCivility(CivilityRepository $civilityRepository, Request $request)
    {
        $civility = new Civility();
        $form = $this->createForm(CivilityType::class, $civility);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($civility);
            $entityManager->flush();
        }

        return $this->render('civility/index.html.twig', [
            'civilities' => $civilityRepository->findAll(),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/new", name="civility_new", methods={"GET","POST"})
     */
    public function newCivility(Request $request)
    {
        $civility = new Civility();
        $form = $this->createForm(CivilityType::class, $civility);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($civility);
            $entityManager->flush();

            return $this->redirectToRoute('civility_index');
        }

        return $this->render('civility/new.html.twig', [
            'civility' => $civility,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="civility_show", methods={"GET"})
     */
    public function showCivility(Civility $civility)
    {
        return $this->render('civility/show.html.twig', [
            'civility' => $civility,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="civility_edit", methods={"GET","POST"})
     */
    public function editCivility(Request $request, Civility $civility)
    {
        $form = $this->createForm(CivilityType::class, $civility);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('civility_index');
        }

        return $this->render('civility/edit.html.twig', [
            'civility' => $civility,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="civility_delete", methods={"DELETE"})
     */
    public function deleteCivility(Request $request, Civility $civility)
    {
        if ($this->isCsrfTokenValid('delete'.$civility->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($civility);
            $entityManager->flush();
        }

        return $this->redirectToRoute('civility_index');
    }
}
