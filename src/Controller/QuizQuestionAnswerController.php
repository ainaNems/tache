<?php

namespace App\Controller;

use App\Entity\QuizQuestionAnswer;
use App\Form\QuizQuestionAnswerType;
use App\Repository\QuizQuestionAnswerRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/quiz-question-answer")
 */
class QuizQuestionAnswerController extends AbstractController
{
    /**
     * @param QuizQuestionAnswerRepository $quizQuestionAnswerRepository
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @Route("/", name="quiz_question_answer_index", methods={"GET","POST"})
     */
    public function index(QuizQuestionAnswerRepository $quizQuestionAnswerRepository,Request $request)
    {
        $quizQuestionAnswer = new QuizQuestionAnswer();
        $form = $this->createForm(QuizQuestionAnswerType::class, $quizQuestionAnswer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($quizQuestionAnswer);
            $entityManager->flush();

            return $this->redirectToRoute('quiz_question_answer_index');
        }

        return $this->render('quiz_question_answer/index.html.twig', [
            'quiz_question_answers' => $quizQuestionAnswerRepository->findAll(),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/new", name="quiz_question_answer_new", methods={"GET","POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function newQuizQuestionAnswer(Request $request)
    {
        $quizQuestionAnswer = new QuizQuestionAnswer();
        $form = $this->createForm(QuizQuestionAnswerType::class, $quizQuestionAnswer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($quizQuestionAnswer);
            $entityManager->flush();

            return $this->redirectToRoute('quiz_question_answer_index');
        }

        return $this->render('quiz_question_answer/new.html.twig', [
            'quiz_question_answer' => $quizQuestionAnswer,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="quiz_question_answer_show", methods={"GET"})
     * @param QuizQuestionAnswer $quizQuestionAnswer
     * @return Response
     */
    public function show(QuizQuestionAnswer $quizQuestionAnswer)
    {
        return $this->render('quiz_question_answer/show.html.twig', [
            'quiz_question_answer' => $quizQuestionAnswer,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="quiz_question_answer_edit", methods={"GET","POST"})
     * @param Request $request
     * @param QuizQuestionAnswer $quizQuestionAnswer
     * @return Response
     */
    public function edit(Request $request, QuizQuestionAnswer $quizQuestionAnswer)
    {
        $form = $this->createForm(QuizQuestionAnswerType::class, $quizQuestionAnswer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('quiz_question_answer_index');
        }

        return $this->render('quiz_question_answer/edit.html.twig', [
            'quiz_question_answer' => $quizQuestionAnswer,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="quiz_question_answer_delete", methods={"DELETE"})
     * @param Request $request
     * @param QuizQuestionAnswer $quizQuestionAnswer
     * @return Response
     */
    public function delete(Request $request, QuizQuestionAnswer $quizQuestionAnswer)
    {
        if ($this->isCsrfTokenValid('delete'.$quizQuestionAnswer->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($quizQuestionAnswer);
            $entityManager->flush();
        }

        return $this->redirectToRoute('quiz_question_answer_index');
    }
}
