<?php

namespace App\Controller;

use App\Entity\HsCandidat;
use App\Form\HsCandidatType;
use App\Repository\HsCandidatRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/hs/candidat")
 */
class HsCandidatController extends AbstractController
{
    /**
     * @Route("/", name="hs_candidat_index", methods={"GET"})
     * @param HsCandidatRepository $hsCandidatRepository
     * @return Response
     */
    public function index(HsCandidatRepository $hsCandidatRepository)
    {
        return $this->render('hs_candidat/index.html.twig', [
            'hs_candidats' => $hsCandidatRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="hs_candidat_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function newHsCandidat(Request $request)
    {
        $hsCandidat = new HsCandidat();
        $form = $this->createForm(HsCandidatType::class, $hsCandidat);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($hsCandidat);
            $entityManager->flush();

            return $this->redirectToRoute('hs_candidat_index');
        }

        return $this->render('hs_candidat/new.html.twig', [
            'hs_candidat' => $hsCandidat,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="hs_candidat_show", methods={"GET"})
     * @param HsCandidat $hsCandidat
     * @return Response
     */
    public function show(HsCandidat $hsCandidat)
    {
        return $this->render('hs_candidat/show.html.twig', [
            'hs_candidat' => $hsCandidat,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="hs_candidat_edit", methods={"GET","POST"})
     * @param Request $request
     * @param HsCandidat $hsCandidat
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function edit(Request $request, HsCandidat $hsCandidat)
    {
        $form = $this->createForm(HsCandidatType::class, $hsCandidat);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('hs_candidat_index');
        }

        return $this->render('hs_candidat/edit.html.twig', [
            'hs_candidat' => $hsCandidat,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="hs_candidat_delete", methods={"DELETE"})
     * @param Request $request
     * @param HsCandidat $hsCandidat
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete(Request $request, HsCandidat $hsCandidat)
    {
        if ($this->isCsrfTokenValid('delete'.$hsCandidat->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($hsCandidat);
            $entityManager->flush();
        }

        return $this->redirectToRoute('hs_candidat_index');
    }
}
