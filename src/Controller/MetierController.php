<?php

namespace App\Controller;

use App\Entity\Metier;
use App\Form\MetierType;
use App\Repository\MetierRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/metier")
 */
class MetierController extends AbstractController
{
    /**
     * @param MetierRepository $metierRepository
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @Route("/", name="metier_index", methods={"GET","POST"})
     */
    public function index(MetierRepository $metierRepository,Request $request)
    {
        $metier = new Metier();
        $form = $this->createForm(MetierType::class, $metier);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($metier);
            $entityManager->flush();

            return $this->redirectToRoute('metier_index');
        }

        return $this->render('metier/index.html.twig', [
            'metiers' => $metierRepository->findAll(),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/new", name="metier_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function newMetier(Request $request)
    {
        $metier = new Metier();
        $form = $this->createForm(MetierType::class, $metier);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($metier);
            $entityManager->flush();

            return $this->redirectToRoute('metier_index');
        }

        return $this->render('metier/new.html.twig', [
            'metier' => $metier,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="metier_show", methods={"GET"})
     * @param Metier $metier
     * @return Response
     */
    public function show(Metier $metier)
    {
        return $this->render('metier/show.html.twig', [
            'metier' => $metier,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="metier_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Metier $metier
     * @return Response
     */
    public function edit(Request $request, Metier $metier)
    {
        $form = $this->createForm(MetierType::class, $metier);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('metier_index');
        }

        return $this->render('metier/edit.html.twig', [
            'metier' => $metier,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/{id}", name="metier_delete", methods={"DELETE"})
     * @param Request $request
     * @param Metier $metier
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteCountry(Request $request, Metier $metier)
    {
        if ($this->isCsrfTokenValid('delete'.$metier->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($metier);
            $entityManager->flush();
        }

        return $this->redirectToRoute('metier_index');
    }
}
