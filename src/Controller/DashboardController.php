<?php

namespace App\Controller;

use App\Entity\Quiz;
use App\Entity\Utilisateur;
use App\Entity\Rule;
use App\Form\QuizType;
use App\Repository\MetierRepository;
use App\Repository\QuizRepository;
use App\Repository\QuizTypeRepository;
use App\Repository\RuleRepository;
use App\Repository\SoftSkillsRepository;
use App\Repository\SectorRepository;
use App\Repository\DomainRepository;
use App\Repository\QuizCandidatResponseRepository;
use App\Repository\QuizQuestionAnswerRepository;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

class DashboardController extends AbstractController
{
    /**
     * @Route("/user/dashboard", name="user_dashboard", methods={"GET","POST"})
     * @param QuizRepository $quizRepository
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function index(RuleRepository $rr,Request $request)
    {

        $user = $this->getUser();
        $user_id = $this->getUser()->getId();
        $metier = $user->getMetier();
        $metier_id = $metier->getId();
        $quizzes = $metier->getQuizzes();
        
        $entityManager = $this->getDoctrine()->getManager();        
                $dql = "SELECT qr.answer_num num ,count(qr.answer_num) occur
                FROM App\Entity\QuizCandidatResponse qcr
                JOIN qcr.quizResponse qr
                JOIN qcr.quizQuestion qq
                JOIN qq.quiz q
                JOIN qcr.user  usr
                where q.id = 1 AND usr.id = $user_id 
                GROUP by qr.answer_num                
               ";
        $query = $entityManager->createQuery($dql);
        $soft_response = $query->getResult();
        $max = 0;
        $rule_id = 0;
        foreach ($soft_response as $soft) {
            if($soft['occur']>$max){
                $max = $soft['occur'];
                $rule_id = $soft['num'];
            }
        }

        return $this->render('dashboard/dashboard_candidat.html.twig', [
            "rule" => $rr->find($rule_id)
        ]);
    }
    
    /**
     * @Route("/admin/dashboard", name="admin_dashboard", methods={"GET","POST"})
     * @param QuizRepository $quizRepository
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function dashboardAdmin(RuleRepository $rr,Request $request,SectorRepository $sr, DomainRepository $dr, MetierRepository $mr)
    {


        return $this->render('dashboard/dashboard_admin.html.twig', [
            "users"   => $users = $this->getDoctrine()->getRepository(Utilisateur::class)
                                    ->createQueryBuilder('user')
                                    ->where('user.username <> :name') 
                                    ->setParameter('name', "admin")
                                    ->getQuery()
                                    ->execute()
                               ,
            "sectors" => $sr->findAll(),
            "domains" => $dr->findAll(),
            "jobs"    => $mr->findAll(),

        ]);
    }

    /**
     * @Route("/admin/evaluation", name="evaluation_dashboard", methods={"GET","POST"})
     * @param QuizRepository $quizRepository
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function evaluation(RuleRepository $rr,Request $request,SectorRepository $sr, DomainRepository $dr, MetierRepository $mr)
    {
        $entityManager = $this->getDoctrine()->getManager(); 
        $users = $this->getDoctrine()->getRepository(Utilisateur::class)
                                    ->createQueryBuilder('user')
                                    ->where('user.username <> :name') 
                                    ->setParameter('name', "admin")
                                    ->getQuery()
                                    ->execute();
                               
        $all_users =[];
        foreach ($users as $user) {
            $user_id = $user->getId();
            $dql = "SELECT qr.answer_num num ,count(qr.answer_num) occur
                FROM App\Entity\QuizCandidatResponse qcr
                JOIN qcr.quizResponse qr
                JOIN qcr.quizQuestion qq
                JOIN qq.quiz q
                JOIN qcr.user  usr
                where q.id = 1 AND usr.id = $user_id 
                GROUP by qr.answer_num
                
               ";
            $query = $entityManager->createQuery($dql);
            $result = $query->getResult();
            $max = 0;
            $rule_id = 0;
            
            foreach ($result as $soft) {
                if($soft['occur']>$max){
                    $max = $soft['occur'];
                    $rule_id = $soft['num'];
                }
            }

            $dql = "SELECT sum(qca.score)/count(qca) as score
                FROM App\Entity\QuizCandidatAnswer qca
                JOIN qca.candidat  usr
                JOIN qca.quiz q
                where q.id <> 1 AND usr.id = $user_id 
                GROUP by qca.date
                Order by qca.date DESC
            ";
            $query = $entityManager->createQuery($dql);
            $result = $query->getResult();

            $all_users []=[
                "user" => $user,
                "rule" => $rr->find($rule_id),
                "score" => empty($result) ? 0 : $result[0]['score']
            ];

        }
        $score = array();
        foreach ($all_users as $key => $row)
        {
            $score[$key] = $row['score'];
        }
        array_multisort($score, SORT_DESC, $all_users);

        return $this->render('evaluation/evaluation.html.twig', [
            "users"   => $all_users,
            "sectors" => $sr->findAll(),
            "domains" => $dr->findAll(),
            "jobs"    => $mr->findAll(),
        ]);
    }

    /**
     * @Route("/admin/candidate/{id}", name="candidate_profile", methods={"GET","POST"})
     * @param QuizRepository $quizRepository
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function candidateProfile(Utilisateur $user ,RuleRepository $rr,Request $request,SoftSkillsRepository $sr)
    {
 
        $user_id = $user->getId();
        $metier = $user->getMetier();
        $metier_id = $metier != null ? $metier->getId() : 0;
        $quizzes = $metier != null ? $metier->getQuizzes(): [];
        $entityManager = $this->getDoctrine()->getManager();

        $dql = "SELECT qr.answer_num num ,count(qr.answer_num) occur
        FROM App\Entity\QuizCandidatResponse qcr
        JOIN qcr.quizResponse qr
        JOIN qcr.quizQuestion qq
        JOIN qq.quiz q
        JOIN qcr.user  usr
        where q.id = 1 AND usr.id = $user_id 
        GROUP by qr.answer_num
        
       ";
        $query = $entityManager->createQuery($dql);
        $soft_response = $query->getResult();
        $max = 0;
        $rule_id = 0;
        
        foreach ($soft_response as $soft) {
            if($soft['occur']>$max){
                $max = $soft['occur'];
                $rule_id = $soft['num'];
            }
        }
        
        $hard_skills = [];

        $skills = $metier != null ? $metier->getHardSkills(): [];
        foreach ( $skills as $skill) {
            $skill_id = $skill->getId();
               $dql = " SELECT sum(hsc.score) / count(hsc) score, hs.hsName , hs.id
               from App\Entity\HardSkillsCandidat hsc
               JOIN hsc.candidat usr
               JOIN hsc.hardSkill as hs
               WHERE usr.id = $user_id and hs.id = $skill_id
               GROUP By hsc.date
               ORDER By hsc.date DESC            
               ";

               $query = $entityManager->createQuery($dql);
               $result = $query->getResult();
               $hard_skills[] = !empty($result) ? $result[0] : [ "score" => 0, "hsName" => $skill->getHsName(), "id" => $skill->getId() ];
        }

        $dql = "SELECT sum(qca.score)/count(qca) as score ,qca.date as date
                FROM App\Entity\QuizCandidatAnswer qca
                JOIN qca.candidat  usr
                JOIN qca.quiz q
                where q.id <> 1 AND usr.id = $user_id 
                GROUP by qca.date
                Order by qca.date ASC
            ";
            $query = $entityManager->createQuery($dql);
            $evolution = $query->getResult();
            

        return $this->render('evaluation/candidate_profile.html.twig',[
            "rule" => $rr->find($rule_id),
            "hard_skills" => $hard_skills,
            "user" => $user,
            "soft_skills" => $sr->findAll(),
            "evolutions" => $evolution
        ]);
    }

    /**
     * @Route("/evolutions", name="skill_evolution", methods={"GET","POST"})
     * @param QuizRepository $quizRepository
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function skillEvolution(Request $request)
    {
        $data = $request->request->all();
        $user_id = $data['user'];
        $skill_id = $data['skill'];
        $entityManager = $this->getDoctrine()->getManager();

        $dql = " SELECT sum(hsc.score) / count(hsc) score, hs.hsName , hsc.date
        from App\Entity\HardSkillsCandidat hsc
        JOIN hsc.candidat usr
        JOIN hsc.hardSkill as hs
        WHERE usr.id = $user_id and hs.id = $skill_id
        GROUP By hsc.date            
        ";

        $query = $entityManager->createQuery($dql);
        $result = $query->getResult();
       

        return new JsonResponse($result);
    }

    
}
