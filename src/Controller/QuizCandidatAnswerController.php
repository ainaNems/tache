<?php

namespace App\Controller;

use App\Entity\QuizCandidatAnswer;
use App\Form\QuizCandidatAnswerType;
use App\Repository\QuizCandidatAnswerRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/quiz/candidat/answer")
 */
class QuizCandidatAnswerController extends AbstractController
{
    /**
     * @Route("/", name="quiz_candidat_answer_index", methods={"GET"})
     */
    public function index(QuizCandidatAnswerRepository $quizCandidatAnswerRepository): Response
    {
        return $this->render('quiz_candidat_answer/index.html.twig', [
            'quiz_candidat_answers' => $quizCandidatAnswerRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="quiz_candidat_answer_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $quizCandidatAnswer = new QuizCandidatAnswer();
        $form = $this->createForm(QuizCandidatAnswerType::class, $quizCandidatAnswer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($quizCandidatAnswer);
            $entityManager->flush();

            return $this->redirectToRoute('quiz_candidat_answer_index');
        }

        return $this->render('quiz_candidat_answer/new.html.twig', [
            'quiz_candidat_answer' => $quizCandidatAnswer,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="quiz_candidat_answer_show", methods={"GET"})
     */
    public function show(QuizCandidatAnswer $quizCandidatAnswer): Response
    {
        return $this->render('quiz_candidat_answer/show.html.twig', [
            'quiz_candidat_answer' => $quizCandidatAnswer,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="quiz_candidat_answer_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, QuizCandidatAnswer $quizCandidatAnswer): Response
    {
        $form = $this->createForm(QuizCandidatAnswerType::class, $quizCandidatAnswer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('quiz_candidat_answer_index');
        }

        return $this->render('quiz_candidat_answer/edit.html.twig', [
            'quiz_candidat_answer' => $quizCandidatAnswer,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="quiz_candidat_answer_delete", methods={"DELETE"})
     */
    public function delete(Request $request, QuizCandidatAnswer $quizCandidatAnswer): Response
    {
        if ($this->isCsrfTokenValid('delete'.$quizCandidatAnswer->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($quizCandidatAnswer);
            $entityManager->flush();
        }

        return $this->redirectToRoute('quiz_candidat_answer_index');
    }
}
