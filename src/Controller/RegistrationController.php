<?php


namespace App\Controller;

use App\Repository\DomainRepository;
use App\Repository\MetierRepository;
use App\Repository\SectorRepository;
use FOS\UserBundle\Controller\RegistrationController as BaseController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class RegistrationController extends BaseController
{
    /**
     * @Route("/create-user", name="utilisateur_create", methods={"POST"})
     */
    public  function createUserAjax(Request $request,SectorRepository $sr, DomainRepository $dr, MetierRepository $mr){
        $_user_manager = $this->get('fos_user.user_manager');
        $_data = $request->request->all();
        $_response = [];
        $_is_email_exists =  $_user_manager->findUserByEmail($_data['email']);

        if($_is_email_exists){
            $_response = ["message" => "Cet email existe déjà"];
            return new JsonResponse($_response);
        }

        $_user = $_user_manager->createUser();

        $_is_user_name_exists =  $_user_manager->findUserByUserName($_data['user_name']);

        if($_is_user_name_exists){
            $_response = ["message" => "Cet pseudo est déjà pris"];
            return new JsonResponse($_response);
        }

        $sector = $sr->find($_data['sector']);
        $domain = $dr->find($_data['domain']);
        $job    = $mr->find($_data['job']);
        $_user->setMetier($job);
        $_user->setSector($sector);
        $_user->setDomain($domain);
        $_user->setUsername($_data['user_name']);
        $_user->setEmail($_data['email']);
        $_user->setEmailCanonical($_data['email']);
       // this method will encrypt the password with the default settings :)
        $_user->setPlainPassword($_data['mdp']);
        $_user->setName($_data['name']);
        $_user->setFirstName($_data['last_name']);
        $_user->addRole('ROLE_CANDIDAT');
        $_user->setEnabled(1);
        $_user_manager->updateUser($_user);

        $_response = ["message" => "OK"];

        return new JsonResponse($_response);
    }

    /**
     * @Route("/profile", name="profile", methods={"GET"})
     */
    public  function settingAction(Request $request, DomainRepository $domainRepository){
        $_user = $this->getUser();
        return $this->render('profile/profile.html.twig', [
            'user' => $_user,
            'domains' => $domainRepository->findAll(),
        ]);
    }


    /**
     * @Route("/get-domains", name="get_metier_by_domain", methods={"POST"})
     * @param Request $request
     * @param DomainRepository $domainRepository
     * @return JsonResponse
     */
    public  function getAllMetierAction(Request $request, DomainRepository $domainRepository){
        $_data = $request->request->all();
        $_html = '';
        if(isset($_data ['domain_id'])){
          $_domain  =   $domainRepository->find($_data['domain_id']);
          $_metiers = $_domain->getMetiers();
          $_render = $this->render('domain/list.metier.html.twig',['metiers' => $_metiers]);
          $_html = $_render->getContent();
        }
        $_response = ['html' => $_html];

        return new JsonResponse($_response);
    }

    /**
     * @Route("/profile-update", name="update_profile", methods={"POST"})
     * @param Request $_request
     * @return JsonResponse
     */
    public function updateUserProfile(Request $_request,DomainRepository $domainRepository,MetierRepository $metierRepository){
        $_user_manager = $this->get('fos_user.user_manager');

        $_data = $_request->request->all();
        $_pics = $_request->files->all();
        $_user = $this->getUser();

        if(isset($_pics['profile_pics'])){
            $_pic_url = $this->moveProfilePics($_pics['profile_pics']);
            if($_pic_url != null )$_user->setPicUrl($_pic_url);
        }

        if(isset($_data['firstName'])){
            $_user->setName($_data['firstName']);
        }
        if(isset($_data['lastName'])){
            $_user->setFirstName($_data['lastName']);
        }
        if(isset($_data['facebook'])){
            $_user->setFacebook($_data['facebook']);
        }
        if(isset($_data['twitter'])){
            $_user->setTwiter($_data['twitter']);
        }
        if(isset($_data['linkedin'])){
            $_user->setLinkedin($_data['linkedin']);
        }
        if(isset($_data['instagram'])){
            $_user->setInstagram($_data['instagram']);
        }
        if(isset($_data['telephone'])){
            $_user->setTelephone($_data['telephone']);
        }
        if(isset($_data['email']) && $_user->getEmail() != $_data['email'] ){
            $_is_email_exists =  $_user_manager->findUserByEmail($_data['email']);

            if($_is_email_exists){
                $_response = ["message" => "Cet email existe déjà"];
                return new JsonResponse($_response);
            }

            $_user->setEmail($_data['email']);
        }
        if(isset($_data['userName']) && $_user->getUserName() != $_data['userName'] ){
            $_is_user_name_exists =  $_user_manager->findUserByUserName($_data['userName']);

            if($_is_user_name_exists){
                $_response = ["message" => "Cet pseudo est déjà prise"];
                return new JsonResponse($_response);
            }

            $_user->setUserName($_data['userName']);
        }
        /**if(isset($_data['domain'])){
            $_domain = $domainRepository->find($_data['domain']);
            if($_domain != null ){
                $_user->setDomain($_domain);
            }
        }
        if(isset($_data['metier'])){
            $_metier = $metierRepository->find($_data['metier']);
            if($_metier != null ){
                $_user->setMetier($_metier);
            }
        }**/

        $_user_manager->updateUser($_user);

        return new JsonResponse(['message'=> 'Mis à jour fait avec succès']);
    }

    /**
     * deplacer la photo de profile dans le dossier uploads
     * @param $_profile_pics
     * @return null|string
     */
    public function moveProfilePics($_profile_pics)
    {
        $_new_file_name     = "";
        if(!is_null($_profile_pics)) {
            $_pics_file_name = pathinfo($_profile_pics->getClientOriginalName(), PATHINFO_FILENAME);
            $_safe_file_name = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $_pics_file_name);
            $_new_file_name  = $_safe_file_name . '-' . uniqid() . '.' . $_profile_pics->guessExtension();
            try {
                $_profile_pics->move(
                    $this->getParameter('pics'),
                    $_new_file_name
                );
            } catch (FileException $e) {
                return null;
            }
        }

        return $_new_file_name;
    }

    /**
     * @Route("sign-in/ajax", name="login_ajax", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function loginAjaxAction(Request $request){
        $user_manager = $this->get('fos_user.user_manager');
        $encoderService = $this->get('security.password_encoder');
        $_data = $request->request->all();

        if( !isset($_data['username']) || !isset($_data['pwd'])){
            return new JsonResponse(['message' => 'Veuilez remplir les champs']);
        }
        $_username = $_data['username'];
        $_password = $_data['pwd'];

        $user = $user_manager->findUserByUsername($_username);

        if(!$user){
            return new JsonResponse(['message' => "Pseudo ou mot de passe invalide"]);
        }

        $salt = $user->getSalt();
        if(!$encoderService->isPasswordValid($user, $_password, $salt)) {
            return new JsonResponse(['message' => "Pseudo ou mot de passe invalide"]);
        }
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $this->get('security.token_storage')->setToken($token);
        $this->get('session')->set('_security_main', serialize($token));
        $request->getSession()->set('_locale', 'en');

        $event = new InteractiveLoginEvent($request, $token);
        $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);

        return new JsonResponse(['message' => 'OK']);
    }

    /**
     * @Route("/sign-in", name="my_login", methods={"POST","GET"})
     * @param Request $_request
     * @return mixed
     */
    public  function renderLogin(Request $_request){
        $user = $this->getUser();
        if($user == null){
            return $this->render('login/loginv1.html.twig');
        }
        if($user->hasRole('ROLE_ADMIN')){
            return $this->redirectToRoute('admin_dashboard');
        }
        if($user->hasRole('ROLE_CANDIDAT')){
            return $this->redirectToRoute('quiz_candidat_response_index');
        }

        return $this->render('login/loginv1.html.twig');
    }
    
    /**
     * @Route("/register", name="registration", methods={"POST","GET"})
     * @param Request $_request
     * @return mixed
     */
    public function index(Request $request,SectorRepository $sr, DomainRepository $dr, MetierRepository $mr){
        return $this->render('registration/home.html.twig',[
            "sectors" => $sr->findAll(),
            "domains" => $dr->findAll(),
            "jobs"    => $mr->findAll(),
        ]);
    }
}