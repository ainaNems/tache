<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        $user = $this->getUser();
        if($user == null){
            return $this->redirectToRoute('my_login');
        }
        if($user->hasRole('ROLE_ADMIN')){
            return $this->redirectToRoute('admin_dashboard');
        }
        if($user->hasRole('ROLE_CANDIDAT')){
            return $this->redirectToRoute('quiz_candidat_response_index');
        }

        return $this->redirectToRoute('my_login');
    }

 /**
     * @Route("/language", name="language")
     */
    public function setLanguage(Request $request)
    {
        $request->getSession()->set('_locale', $request->query->get('lang'));       

        return $this->redirectToRoute('home');
    }
       
}
