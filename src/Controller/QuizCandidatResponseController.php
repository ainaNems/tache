<?php

namespace App\Controller;

use App\Entity\QuizCandidatAnswer;
use App\Entity\QuizCandidatResponse;
use App\Entity\QuizQuestionAnswer;
use App\Entity\HardSkillsCandidat;
use App\Form\QuizCandidatResponseType;
use App\Repository\MetierRepository;
use App\Repository\QuizCandidatResponseRepository;
use App\Repository\QuizQuestionAnswerRepository;
use App\Repository\QuizQuestionRepository;
use App\Repository\SoftSkillsRepository;
use App\Repository\QuizRepository;
use App\Repository\RuleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Contracts\Translation\TranslatorInterface;


class QuizCandidatResponseController extends AbstractController
{
    /**
     * @Route("/candidate", name="quiz_candidat_response_index", methods={"GET"})
     * @param QuizCandidatResponseRepository $quizCandidatResponseRepository
     * @return Response
     */
    public function index(QuizCandidatResponseRepository $quizCandidatResponseRepository, MetierRepository $metierRepository,QuizRepository $qr, RuleRepository $rr,SoftSkillsRepository $sr)
   {
        $user = $this->getUser();
        $user_id = $this->getUser()->getId();
        $metier = $user->getMetier();
        $metier_id = $metier->getId();
        $quizzes = $metier->getQuizzes();
        $entityManager = $this->getDoctrine()->getManager();
        
        $dql = "SELECT max(qca.date) as date 
                FROM App\Entity\QuizCandidatAnswer qca
                JOIN qca.candidat  usr
                JOIN qca.quiz quiz
                where usr.id = $user_id and quiz.id <> 1
                GROUP BY quiz.id Order By qca.date ASC
               ";

        $query = $entityManager->createQuery($dql);
        $dates = $query->getResult();
        $length = count($dates);
        $last_date = 0;
        $start_date = 0;
        if($length > 0 )
        { 
            $last_date = $dates[$length-1]['date'] != null ? explode(' ',$query->getResult()[$length-1]['date'])[0]: 0;
            $last_date = $dates[0]['date'] != null ? explode(' ',$query->getResult()[0]['date'])[0]: 0;
        }

        $dql = "SELECT q as quiz, qca.score as score , qca.date as date
                FROM App\Entity\Quiz q
                JOIN q.metiers m 
                JOIN q.quizCandidatAnswers qca
                JOIN qca.candidat  usr
                where m.id = $metier_id AND usr.id = $user_id  
                GROUP By q.id  ,qca.date
               ";

        $query = $entityManager->createQuery($dql);
        $quizzes_complete = $query->getResult();

        $dql = "SELECT q 
                FROM App\Entity\Quiz q
                JOIN q.metiers m 
                JOIN m.utilisateurs usr
                where usr.id = $user_id 
               ";

        $query = $entityManager->createQuery($dql);
        $quizzes = $query->getResult();

        $dql = "SELECT qr.answer_num num ,count(qr.answer_num) occur
        FROM App\Entity\QuizCandidatResponse qcr
        JOIN qcr.quizResponse qr
        JOIN qcr.quizQuestion qq
        JOIN qq.quiz q
        JOIN qcr.user  usr
        where q.id = 1 AND usr.id = $user_id 
        GROUP by qr.answer_num        
       ";

        $query = $entityManager->createQuery($dql);
        $soft_response = $query->getResult();
        
        $max = 0;
        $rule_id = 0;
        
        foreach ($soft_response as $soft) {
            if($soft['occur']>$max){
                $max = $soft['occur'];
                $rule_id = $soft['num'];
            }
        }
        
        $hard_skills = [];

        $skills = $metier->getHardSkills();
        foreach ( $skills as $skill) {
            $skill_id = $skill->getId();
               $dql = " SELECT sum(hsc.score) / count(hsc) score, hs.hsName , hs.id
               from App\Entity\HardSkillsCandidat hsc
               JOIN hsc.candidat usr
               JOIN hsc.hardSkill as hs
               WHERE usr.id = $user_id and hs.id = $skill_id
               and hsc.date = '$last_date'            
               ";

               $query = $entityManager->createQuery($dql);
               $result = $query->getResult();
               $hard_skills[] = $result[0];
        }
       
            $dql = "SELECT sum(qca.score)/count(qca) as score ,qca.date as date
                FROM App\Entity\QuizCandidatAnswer qca
                JOIN qca.candidat  usr
                JOIN qca.quiz q
                where q.id <> 1 AND usr.id = $user_id 
                GROUP by qca.date
                Order by qca.date ASC
            ";
            $query = $entityManager->createQuery($dql);
            $evolution = $query->getResult();
            
        return $this->render('game/quiz_template.html.twig',[
            "quizzes_complete" =>$quizzes_complete,
            "quizzes" => $quizzes,
            "soft_quizzes" => $qr->find(1),
            "rule" => $rr->find($rule_id),
            "hard_skills" => $hard_skills,
            "user" => $user,
            "last_date" => $last_date,
            "evolutions"=> $evolution,
            "soft_skills"=> $sr->findAll()
        ]);
    }

    /**
     * @Route("/save-quiz-response", name="quiz_candidat_response_save", methods={"POST"})
     * @param Request $request
     * @param QuizRepository $quizRepository
     * @param QuizQuestionRepository $quizQuestionRepository
     * @param QuizQuestionAnswerRepository $quizQuestionAnswerRepository
     * @return JsonResponse
     */
    public  function saveCandidatResponnseAjax(Request $request,QuizRepository $quizRepository, QuizQuestionRepository $quizQuestionRepository, QuizQuestionAnswerRepository $quizQuestionAnswerRepository){
        $data = $request->request->all();
        $id = $data['id'];
        $score = $data['score'];
        $reponses = $data['response'];
        $entityManager = $this->getDoctrine()->getManager();

        $quiz = $quizRepository->find($id);
        $score = 0;
        if($quiz != null){
            $user = $this->getUser();
            $quiz_candidat_answer = new QuizCandidatAnswer();
            $quiz_candidat_answer->setScore($score);
            $quiz_candidat_answer->setCandidat($user);
            $quiz_candidat_answer->setQuiz($quiz);
            $quiz_candidat_answer->setDate(new \DateTime(date('Y-m-d')));
            $entityManager->persist($quiz_candidat_answer);
            $entityManager->flush();

            foreach ($reponses as $reponse){
                $question_id = $reponse['question'];
                $answer_id   = $reponse['answer'];

                $question = $quizQuestionRepository->find($question_id);
                $answer = $quizQuestionAnswerRepository->find($answer_id);

                if($question != null && $answer != null){
                    $skills  = $question->getHardSkills();

                    $score += $answer->getScore();
                    $candidate_response = new QuizCandidatResponse();
                    $candidate_response->setUser($user);
                    $candidate_response->setQuizCandidatAnswer($quiz_candidat_answer);
                    $candidate_response->setQuizQuestion($question);
                    $candidate_response->setQuizResponse($answer);
                    $candidate_response->setDate(new \DateTime(date('Y-m-d')));
                    $entityManager->persist($candidate_response);
                    $entityManager->flush();

                    foreach ($skills as $skill) {
                        $hardSkillCandidat = new HardSkillsCandidat();
                        $hardSkillCandidat->setCandidat($user);
                        $hardSkillCandidat->setScore($answer->getScore());
                        $hardSkillCandidat->setHardSkill($skill);
                        $hardSkillCandidat->setDate(new \DateTime(date('Y-m-d')));
                        $entityManager->persist($hardSkillCandidat);
                        $entityManager->flush();
                    }
                }
            }
            
            $score = round($score / count($quiz->getQuizQuestions()),1);
            $quiz_candidat_answer->setScore($score);
            $entityManager->flush();

            return new JsonResponse(["message" => "OK"]);
        }

        return new JsonResponse(["message" => "KO"]);
    }

    /**
     * @Route("/test", name="test", methods={"GET"})
     * @return mixed
     */
    public function testAction(){
        return $this->render('architect/architect.html.twig');
    }

    /**
     * @Route("/candidate/play", name="play", methods={"GET"})
     * @param QuizCandidatResponseRepository $quizCandidatResponseRepository
     * @return Response
     */
    public function dashboardCandidat(QuizCandidatResponseRepository $quizCandidatResponseRepository, MetierRepository $metierRepository,Request $request,QuizRepository $qr, RuleRepository $rr,TranslatorInterface $translator,SoftSkillsRepository $sr)
   {
        $user = $this->getUser();
        $user_id = $this->getUser()->getId();
        $metier = $user->getMetier();
        $metier_id = $metier->getId();
        $quizzes = $metier->getQuizzes();
        $entityManager = $this->getDoctrine()->getManager();
        
        $dql = "SELECT max(qca.date) as date 
                FROM App\Entity\QuizCandidatAnswer qca
                JOIN qca.candidat  usr
                JOIN qca.quiz quiz
                where usr.id = $user_id and quiz.id <> 1
                GROUP BY quiz.id Order By qca.date ASC
               ";

        $query = $entityManager->createQuery($dql);
        $dates = $query->getResult();
        $length = count($dates);
        $last_date = 0;
        $start_date = 0;
        if($length > 0 )
        { 
            $last_date = $dates[$length-1]['date'] != null ? explode(' ',$query->getResult()[$length-1]['date'])[0]: 0;
            $last_date = $dates[0]['date'] != null ? explode(' ',$query->getResult()[0]['date'])[0]: 0;
        }

        $dql = "SELECT q as quiz, qca.score as score , qca.date as date
                FROM App\Entity\Quiz q
                JOIN q.metiers m 
                JOIN q.quizCandidatAnswers qca
                JOIN qca.candidat  usr
                where m.id = $metier_id AND usr.id = $user_id  
                GROUP By q.id  ,qca.date
               ";

        $query = $entityManager->createQuery($dql);
        $quizzes_complete = $query->getResult();

        $dql = "SELECT q 
                FROM App\Entity\Quiz q
                JOIN q.metiers m 
                JOIN m.utilisateurs usr
                where usr.id = $user_id 
               ";

        $query = $entityManager->createQuery($dql);
        $quizzes = $query->getResult();

        $dql = "SELECT qr.answer_num num ,count(qr.answer_num) occur
        FROM App\Entity\QuizCandidatResponse qcr
        JOIN qcr.quizResponse qr
        JOIN qcr.quizQuestion qq
        JOIN qq.quiz q
        JOIN qcr.user  usr
        where q.id = 1 AND usr.id = $user_id 
        GROUP by qr.answer_num        
       ";

        $query = $entityManager->createQuery($dql);
        $soft_response = $query->getResult();
        
        $max = 0;
        $rule_id = 0;
        
        foreach ($soft_response as $soft) {
            if($soft['occur']>$max){
                $max = $soft['occur'];
                $rule_id = $soft['num'];
            }
        }
        
        $hard_skills = [];

        $skills = $metier->getHardSkills();
        foreach ( $skills as $skill) {
            $skill_id = $skill->getId();
               $dql = " SELECT sum(hsc.score) / count(hsc) score, hs.hsName , hs.id
               from App\Entity\HardSkillsCandidat hsc
               JOIN hsc.candidat usr
               JOIN hsc.hardSkill as hs
               WHERE usr.id = $user_id and hs.id = $skill_id
               and hsc.date = '$last_date'            
               ";

               $query = $entityManager->createQuery($dql);
               $result = $query->getResult();
               $hard_skills[] = $result[0];
        }
       
            $dql = "SELECT sum(qca.score)/count(qca) as score ,qca.date as date
                FROM App\Entity\QuizCandidatAnswer qca
                JOIN qca.candidat  usr
                JOIN qca.quiz q
                where q.id <> 1 AND usr.id = $user_id 
                GROUP by qca.date
                Order by qca.date ASC
            ";
            $query = $entityManager->createQuery($dql);
            $evolution = $query->getResult();
            
        return $this->render('game/play.html.twig',[
            "quizzes_complete" =>$quizzes_complete,
            "quizzes" => $quizzes,
            "soft_quizzes" => $qr->find(1),
            "rule" => $rr->find($rule_id),
            "hard_skills" => $hard_skills,
            "user" => $user,
            "last_date" => $last_date,
            "evolutions"=> $evolution,
            "soft_skills"=> $sr->findAll()
        ]);
    }


}