<?php

namespace App\Controller;

use App\Entity\QuizSs;
use App\Form\QuizSsType;
use App\Repository\QuizRepository;
use App\Repository\SoftSkillsRepository;
use App\Repository\RuleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/quiz-ss")
 */
class QuizSsController extends AbstractController
{
    /**
     * @Route("/", name="quiz_ss_index", methods={"GET"})
     */
    public function index(SoftSkillsRepository $softSkillsRepository,QuizRepository $qr,RuleRepository $rr)
    {
        return $this->render('quiz_ss/index.html.twig', [
            'soft_skills' => $softSkillsRepository->findAll(),
            'questions' => $qr->find(1)->getQuizQuestions(),
            'rule1_soft_skills'=> $rr->find(1)->getSoftSkills(),
            'rule2_soft_skills'=> $rr->find(2)->getSoftSkills(),
            'rule3_soft_skills'=> $rr->find(3)->getSoftSkills(),
            'rule4_soft_skills'=> $rr->find(4)->getSoftSkills(),
            'page' => 0,
            "message" => "No message" 
        ]);
    }

    /**
     * @Route("/new", name="quiz_ss_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $quizSs = new QuizSs();
        $form = $this->createForm(QuizSsType::class, $quizSs);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($quizSs);
            $entityManager->flush();

            return $this->redirectToRoute('quiz_ss_index');
        }

        return $this->render('quiz_ss/new.html.twig', [
            'quiz_ss' => $quizSs,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="quiz_ss_show", methods={"GET"})
     */
    public function show(QuizSs $quizSs): Response
    {
        return $this->render('quiz_ss/show.html.twig', [
            'quiz_ss' => $quizSs,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="quiz_ss_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, QuizSs $quizSs): Response
    {
        $form = $this->createForm(QuizSsType::class, $quizSs);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('quiz_ss_index');
        }

        return $this->render('quiz_ss/edit.html.twig', [
            'quiz_ss' => $quizSs,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="quiz_ss_delete", methods={"DELETE"})
     */
    public function delete(Request $request, QuizSs $quizSs): Response
    {
        if ($this->isCsrfTokenValid('delete'.$quizSs->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($quizSs);
            $entityManager->flush();
        }

        return $this->redirectToRoute('quiz_ss_index');
    }
}
