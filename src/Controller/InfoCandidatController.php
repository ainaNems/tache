<?php

namespace App\Controller;

use App\Entity\InfoCandidat;
use App\Form\InfoCandidatType;
use App\Repository\InfoCandidatRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/info/candidat")
 */
class InfoCandidatController extends AbstractController
{
    /**
     * @Route("/", name="info_candidat_index", methods={"GET","POST"})
     * @param InfoCandidatRepository $infoCandidatRepository
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function index(InfoCandidatRepository $infoCandidatRepository,Request $request)
    {
        $infoCandidat = new InfoCandidat();
        $form = $this->createForm(InfoCandidatType::class, $infoCandidat);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($infoCandidat);
            $entityManager->flush();

            return $this->redirectToRoute('info_candidat_index');
        }

        return $this->render('info_candidat/index.html.twig', [
            'info_candidats' => $infoCandidatRepository->findAll(),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @Route("/new", name="info_candidat_new", methods={"GET","POST"})
     */
    public function newInfoCandidat(Request $request)
    {
        $infoCandidat = new InfoCandidat();
        $form = $this->createForm(InfoCandidatType::class, $infoCandidat);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($infoCandidat);
            $entityManager->flush();

            return $this->redirectToRoute('info_candidat_index');
        }

        return $this->render('info_candidat/new.html.twig', [
            'info_candidat' => $infoCandidat,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param InfoCandidat $infoCandidat
     * @return Response
     * @Route("/{id}", name="info_candidat_show", methods={"GET"})
     */
    public function show(InfoCandidat $infoCandidat)
    {
        return $this->render('info_candidat/show.html.twig', [
            'info_candidat' => $infoCandidat,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="info_candidat_edit", methods={"GET","POST"})
     * @param Request $request
     * @param InfoCandidat $infoCandidat
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function edit(Request $request, InfoCandidat $infoCandidat)
    {
        $form = $this->createForm(InfoCandidatType::class, $infoCandidat);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('info_candidat_index');
        }

        return $this->render('info_candidat/edit.html.twig', [
            'info_candidat' => $infoCandidat,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Request $request
     * @param InfoCandidat $infoCandidat
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/{id}", name="info_candidat_delete", methods={"DELETE"})
     */
    public function delete(Request $request, InfoCandidat $infoCandidat)
    {
        if ($this->isCsrfTokenValid('delete'.$infoCandidat->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($infoCandidat);
            $entityManager->flush();
        }

        return $this->redirectToRoute('info_candidat_index');
    }
}
