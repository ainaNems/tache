<?php

namespace App\Controller;

use App\Entity\Quiz;
use App\Entity\Rule;
use App\Form\QuizType;
use App\Repository\MetierRepository;
use App\Repository\QuizRepository;
use App\Repository\QuizTypeRepository;
use App\Repository\RuleRepository;
use App\Repository\SoftSkillsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
/**
 * @Route("/quiz")
 */
class QuizController extends AbstractController
{
    /**
     * @Route("/", name="quiz_index", methods={"GET","POST"})
     * @param QuizRepository $quizRepository
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function index(QuizRepository $quizRepository,Request $request)
    {
        $quiz = new Quiz();
        $form = $this->createForm(QuizType::class, $quiz);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($quiz);
            $entityManager->flush();

            return $this->redirectToRoute('quiz_index');
        }

        return $this->render('quiz/index.html.twig', [
            'quizzes' => $quizRepository->findAll(),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/new", name="quiz_new", methods={"GET","POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function newQuiz(Request $request,QuizTypeRepository $qtr)
    {
        $quiz = new Quiz();
        $form = $this->createForm(QuizType::class, $quiz);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $type = $qtr->find(1);
            $quiz->setQuizType($type);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($quiz);
            $entityManager->flush();

            return $this->redirectToRoute('quiz_index');
        }

        return $this->render('quiz/new.html.twig', [
            'quiz' => $quiz,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/new-ss", name="quizz_ss_new", methods={"GET","POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function newQuizSS(Request $request,QuizRepository $qr ,RuleRepository $rr,SoftSkillsRepository $ssr)
    {
        $data = $request->request->all();
        $entityManager = $this->getDoctrine()->getManager();

        $rule1 = $rr->find(1);
        $rule2 = $rr->find(2);
        $rule3 = $rr->find(3);
        $rule4 = $rr->find(4);
        $this->removeRuleSoftSkills($rule1);
        $this->removeRuleSoftSkills($rule2);
        $this->removeRuleSoftSkills($rule3);
        $this->removeRuleSoftSkills($rule4);
        
        if(isset($data["quiz_ss_1"])){
            foreach ($data["quiz_ss_1"] as $soft_id) {
                $soft_skill = $ssr->find($soft_id);
                if($soft_skill != null){
                    $rule1->addSoftSkill($soft_skill);
                }
                $entityManager->flush();
            }
        }
        if(isset($data["quiz_ss_2"])){
            foreach ($data["quiz_ss_2"] as $soft_id) {
                $soft_skill = $ssr->find($soft_id);
                if($soft_skill != null){
                    $rule2->addSoftSkill($soft_skill);
                }
                $entityManager->flush();
            }
        }
        if(isset($data["quiz_ss_3"])){
            foreach ($data["quiz_ss_3"] as $soft_id) {
                $soft_skill = $ssr->find($soft_id);
                if($soft_skill != null){
                    $rule3->addSoftSkill($soft_skill);
                }
                $entityManager->flush();
            }
        }
        if(isset($data["quiz_ss_4"])){
            foreach ($data["quiz_ss_4"] as $soft_id) {
                $soft_skill = $ssr->find($soft_id);
                if($soft_skill != null){
                    $rule4->addSoftSkill($soft_skill);
                }
                $entityManager->flush();
            }
        }

        return $this->redirectToRoute('quiz_ss_index',
        [
            'soft_skills' => $ssr->findAll(),
            'questions' => $qr->find(1)->getQuizQuestions(),
            'rule1_soft_skills'=> $rr->find(1)->getSoftSkills(),
            'rule2_soft_skills'=> $rr->find(2)->getSoftSkills(),
            'rule3_soft_skills'=> $rr->find(3)->getSoftSkills(),
            'rule4_soft_skills'=> $rr->find(4)->getSoftSkills(),
            'page' => 1,
            "message" => "Ajout fait avec succès" 
        ]);
    }

    public function removeRuleSoftSkills($rule){
        $skills = $rule->getSoftSkills();
        foreach($skills as $skill){
            $rule->removeSoftSkill($skill);
        }
    }
    /**
     * @Route("/{id}", name="quiz_show", methods={"GET"})
     * @param Quiz $quiz
     * @return Response
     */
    public function show(Quiz $quiz)
    {
        return $this->render('quiz/show.html.twig', [
            'quiz' => $quiz,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="quiz_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Quiz $quiz
     * @return Response
     */
    public function edit(Request $request, Quiz $quiz): Response
    {
        $_metiers = $quiz->getMetiers();
        $_hard_skills = [];
        foreach ($_metiers as $_metier){
            $_hards = $_metier->getHardSkills();
            foreach ($_hards as $_hard){
                $_hard_skills[]= $_hard;
            }
        }
    
        return $this->render('quiz/edit.html.twig', [
            'hard_skills' => array_unique($_hard_skills),
            'quizz' => $quiz,

            'metiers' => $quiz->getMetiers(),
            'questions' => $quiz->getQuizQuestions()
        ]);
    }

    /**
     * @Route("/{id}", name="quiz_delete", methods={"DELETE"})
     * @param Request $request
     * @param Quiz $quiz
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete(Request $request, Quiz $quiz)
    {
        if ($this->isCsrfTokenValid('delete'.$quiz->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($quiz);
            $entityManager->flush();
        }

        return $this->redirectToRoute('quiz_index');
    }

    /**
     * @Route("/quiz-hard-skills", name="quiz_hard_skills", methods={"POST"})
     * @param Request $request
     * @param QuizRepository $quizRepository
     * @return JsonResponse
     */
    public function getListHardSkillsByQuizIdAction(Request $request,QuizRepository $quizRepository){
        $_data = $request->request->get("quiz");
        if(isset($_data)){
            $_quiz = $quizRepository->find($_data);
            $_html = '';

            $_hard_skills = [];
            if($_quiz != null){
                $_metiers = $_quiz->getMetiers();

                foreach ($_metiers as $_metier){
                    $_hards = $_metier->getHardSkills();
                    foreach ($_hards as $_hard){
                        $_hard_skills[]= $_hard;
                    }
                }
            }
            $_response = $this->render("quiz_question/list_hard_skills.html.twig",["hard_skills" => array_unique($_hard_skills)]);
            $_html = $_response->getContent();

            return new JsonResponse(["message" =>"ok","html"=>$_html]);
        }

        return new JsonResponse(["message" =>"invalid","html"=>""]);
    }

    /**
     * @Route("/quiz-questions", name="quiz_questions", methods={"POST"})
     * @param Request $request
     * @param QuizRepository $quizRepository
     * @return JsonResponse
     */
    public function getListQuestionAction(Request $request,QuizRepository $quizRepository){
        $_data = $request->request->get("quiz");
        if(isset($_data)){
            $_quiz = $quizRepository->find($_data);
            $_html = '';

            $_questions = [];
            if($_quiz != null){
                $_questions = $_quiz->getQuizQuestions();

            }
            $_response = $this->render("quiz_question/list_part.html.twig",["questions" => $_questions]);
            $_html = $_response->getContent();

            return new JsonResponse(["message" =>"ok","html"=>$_html]);
        }

        return new JsonResponse(["message" =>"invalid","html"=>""]);
    }


}
