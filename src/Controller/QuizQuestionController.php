<?php

namespace App\Controller;

use App\Entity\QuizQuestion;
use App\Entity\QuizQuestionAnswer;
use App\Form\QuizQuestionType;
use App\Repository\HardSkillsRepository;
use App\Repository\SoftSkillsRepository;
use App\Repository\RuleRepository;
use App\Repository\QuizQuestionAnswerRepository;
use App\Repository\QuizQuestionRepository;
use App\Repository\QuizRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/quiz-question")
 */
class QuizQuestionController extends AbstractController
{
    /**
     * @Route("/", name="quiz_question_index", methods={"GET","POST"})
     * @param QuizQuestionRepository $quizQuestionRepository
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function index(QuizQuestionRepository $quizQuestionRepository,Request $request)
    {
        $quizQuestion = new QuizQuestion();
        $form = $this->createForm(QuizQuestionType::class, $quizQuestion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($quizQuestion);
            $entityManager->flush();

            return $this->redirectToRoute('quiz_question_index');
        }

        return $this->render('quiz_question/index.html.twig', [
            'quiz_questions' => $quizQuestionRepository->findAll(),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/new", name="quiz_question_new", methods={"GET","POST"})
     * @param Request $request
     * @param QuizRepository $quizRepository
     * @return JsonResponse
     */
    public function newQuizQuestion(Request $request,QuizRepository $quizRepository,HardSkillsRepository $hardSkillsRepository)
    {
        $data = $request->request->all();
        $question = $data['question'];
        $quiz = $data['quiz'];
        $answers = $data['answers'];
        $hard_skills = $data['hs'];
        $new_question = new QuizQuestion();
        $new_question->setQuestion($question);

        $quiz = $quizRepository->find($quiz);
        if($quiz == null){
            $reponse = ['reponse' => "On a pas pu sauvegarder cette...."];
            return new JsonResponse(json_encode($reponse,true));
        }

        $new_question->setQuiz($quiz);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($new_question);
        $entityManager->flush();
        $num = 1;
        foreach ($answers as $answer){
            $new_answuer = new QuizQuestionAnswer();
            $new_answuer->setQuizQuestion($new_question);
            $new_answuer->setAnswer($answer['reponse']);
            $new_answuer->setScore($answer['score']);
            $new_answuer->setAnswerNum($num);
            $entityManager->persist($new_answuer);
            $entityManager->flush();
            $num++;
        }

        foreach ($hard_skills as $hard_skill){
            $_hard = $hardSkillsRepository->find($hard_skill);
            if($_hard != null){
                $new_question->addHardSkill($_hard);
            }
            $entityManager->flush();
        }
        $reponse = ['reponse' => "Sauvegarde fait avec succès"];
        return new JsonResponse(json_encode($reponse,true));
    }

/**
     * @Route("/new-ss", name="quiz_ss_question_new", methods={"GET","POST"})
     * @param Request $request
     * @param QuizRepository $quizRepository
     */
    public function newQuizSsQuestion(Request $request,QuizRepository $qr,RuleRepository $rr,SoftSkillsRepository $ssr,QuizQuestionRepository $qqr)
    {
        $data = $request->request->all();
        $question = $data['question'];
        $answer1 = $data['answer1'];
        $answer2 = $data['answer2'];
        $answer3 = $data['answer3'];
        $answer4 = $data['answer4'];

        $new_question = new QuizQuestion();
        $new_question->setQuestion($question);

        $quiz = $qr->find(1);

        if($quiz == null){
             return $this->redirectToRoute('quiz_ss_index',
        [
            'soft_skills' => $ssr->findAll(),
            'questions' => $qr->find(1)->getQuizQuestions(),
            'rule1_soft_skills'=> $rr->find(1)->getSoftSkills(),
            'rule2_soft_skills'=> $rr->find(2)->getSoftSkills(),
            'rule3_soft_skills'=> $rr->find(3)->getSoftSkills(),
            'rule4_soft_skills'=> $rr->find(4)->getSoftSkills(),
        ]);
        }

        $new_question->setQuiz($quiz);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($new_question);
        $entityManager->flush();

            $new_answuer = new QuizQuestionAnswer();
            $new_answuer->setQuizQuestion($new_question);
            $new_answuer->setAnswer($answer1);
            $new_answuer->setScore(0);
            $new_answuer->setAnswerNum(1);
            $entityManager->persist($new_answuer);
            $entityManager->flush();
            $new_answuer = new QuizQuestionAnswer();
            $new_answuer->setQuizQuestion($new_question);
            $new_answuer->setAnswer($answer2);
            $new_answuer->setScore(0);
            $new_answuer->setAnswerNum(2);
            $entityManager->persist($new_answuer);
            $entityManager->flush();
            $new_answuer = new QuizQuestionAnswer();
            $new_answuer->setQuizQuestion($new_question);
            $new_answuer->setAnswer($answer3);
            $new_answuer->setScore(0);
            $new_answuer->setAnswerNum(3);
            $entityManager->persist($new_answuer);
            $entityManager->flush();
            $new_answuer = new QuizQuestionAnswer();
            $new_answuer->setQuizQuestion($new_question);
            $new_answuer->setAnswer($answer4);
            $new_answuer->setScore(0);
            $new_answuer->setAnswerNum(4);
            $entityManager->persist($new_answuer);
            $entityManager->flush();

        return $this->render('quiz_ss/index.html.twig',
        [
            'soft_skills' => $ssr->findAll(),
            'questions' => $qr->find(1)->getQuizQuestions(),
            'rule1_soft_skills'=> $rr->find(1)->getSoftSkills(),
            'rule2_soft_skills'=> $rr->find(2)->getSoftSkills(),
            'rule3_soft_skills'=> $rr->find(3)->getSoftSkills(),
            'rule4_soft_skills'=> $rr->find(4)->getSoftSkills(),
            'page' => 1,
            "message" => "Ajout fait avec succès" 
        ]);
    }

    /**
     * @Route("/formulaire", name="quiz_question_with_answer", methods={"GET"})
     * @return Response
     */
    public function newFormulaireAction(QuizQuestionRepository $quizQuestionRepository,QuizRepository $quizRepository)
    {
        return $this->render('quiz_question/question.answer.html.twig',[
            "questions" => $quizQuestionRepository->findAll(),
            "quizzes" => $quizRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="quiz_question_edit", methods={"GET","POST"})
     * @param Request $request
     * @param QuizQuestion $quizQuestion
     * @return Response
     */
    public function editAction(Request $request, QuizQuestion $quizQuestion)
    {
        $form = $this->createForm(QuizQuestionType::class, $quizQuestion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('quiz_question_index');
        }

        return $this->render('quiz_question/edit.html.twig', [
            'quiz_question' => $quizQuestion,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="quiz_question_delete", methods={"DELETE"})
     * @param Request $request
     * @param QuizQuestion $quizQuestion
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, QuizQuestion $quizQuestion)
    {
        if ($this->isCsrfTokenValid('delete'.$quizQuestion->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($quizQuestion);
            $entityManager->flush();
        }

        return $this->redirectToRoute('quiz_question_index');
    }

    /**
     * @Route("/{id}", name="quiz_ss_question_delete", methods={"GET"})
     * @param Request $request
     * @param QuizQuestion $quizQuestion
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteSSAction(Request $request, QuizQuestion $quizQuestion,QuizRepository $qr ,RuleRepository $rr,SoftSkillsRepository $ssr)
    {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($quizQuestion);
            $entityManager->flush();
            

        return $this->render('quiz_ss/index.html.twig',
        [
            'soft_skills' => $ssr->findAll(),
            'questions' => $qr->find(1)->getQuizQuestions(),
            'rule1_soft_skills'=> $rr->find(1)->getSoftSkills(),
            'rule2_soft_skills'=> $rr->find(2)->getSoftSkills(),
            'rule3_soft_skills'=> $rr->find(3)->getSoftSkills(),
            'rule4_soft_skills'=> $rr->find(4)->getSoftSkills(),
            'page' => 1,
            "message" => "Suppression fait avec succès" 
        ]);
    }

    /**
     * @Route("/delete-ajax", name="quiz_question_delete_ajax", methods={"POST"})
     * @param Request $request
     * @param QuizQuestion $quizQuestion
     */
    public function deleteAjaxAction(Request $request, QuizQuestionRepository $qqr)
    {
        $data = $request->request->get('question');
        $quizQuestion = $qqr->find($data);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($quizQuestion);
            $entityManager->flush();
        

        return new JsonResponse(['message' => "Suppression avec succès"]);
    }

    /**
     * @Route("/get-form", name="quiz_question_form_ajax", methods={"POST"})
     * @param Request $request
     * @param QuizQuestion $quizQuestion
     */
    public function getFormAjaxAction(Request $request, QuizQuestionRepository $qqr)
    {
        $data = $request->request->get('question');
        $quizQuestion = $qqr->find($data);
        
        $form = $this->render('quiz/show.html.twig',
            [
                "question" => $quizQuestion,
        ]);
        
        $html = $form->getContent();

        return new JsonResponse(['form' => $html]);
    }
}
