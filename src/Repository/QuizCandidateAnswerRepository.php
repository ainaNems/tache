<?php

namespace App\Repository;

use App\Entity\QuizCandidateAnswer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method QuizCandidateAnswer|null find($id, $lockMode = null, $lockVersion = null)
 * @method QuizCandidateAnswer|null findOneBy(array $criteria, array $orderBy = null)
 * @method QuizCandidateAnswer[]    findAll()
 * @method QuizCandidateAnswer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QuizCandidateAnswerRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, QuizCandidateAnswer::class);
    }

    // /**
    //  * @return QuizCandidateAnswer[] Returns an array of QuizCandidateAnswer objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('q.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?QuizCandidateAnswer
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
