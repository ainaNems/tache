<?php

namespace App\Repository;

use App\Entity\RuleDescription;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method RuleDescription|null find($id, $lockMode = null, $lockVersion = null)
 * @method RuleDescription|null findOneBy(array $criteria, array $orderBy = null)
 * @method RuleDescription[]    findAll()
 * @method RuleDescription[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RuleDescriptionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, RuleDescription::class);
    }

    // /**
    //  * @return RuleDescription[] Returns an array of RuleDescription objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RuleDescription
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
