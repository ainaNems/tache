<?php

namespace App\Repository;

use App\Entity\CandidatQuiz;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CandidatQuiz|null find($id, $lockMode = null, $lockVersion = null)
 * @method CandidatQuiz|null findOneBy(array $criteria, array $orderBy = null)
 * @method CandidatQuiz[]    findAll()
 * @method CandidatQuiz[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CandidatQuizRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CandidatQuiz::class);
    }

    // /**
    //  * @return CandidatQuiz[] Returns an array of CandidatQuiz objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CandidatQuiz
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
