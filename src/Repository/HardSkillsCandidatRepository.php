<?php

namespace App\Repository;

use App\Entity\HardSkillsCandidat;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method HardSkillsCandidat|null find($id, $lockMode = null, $lockVersion = null)
 * @method HardSkillsCandidat|null findOneBy(array $criteria, array $orderBy = null)
 * @method HardSkillsCandidat[]    findAll()
 * @method HardSkillsCandidat[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HardSkillsCandidatRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, HardSkillsCandidat::class);
    }

    // /**
    //  * @return HardSkillsCandidat[] Returns an array of HardSkillsCandidat objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?HardSkillsCandidat
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
