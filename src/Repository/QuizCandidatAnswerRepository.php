<?php

namespace App\Repository;

use App\Entity\QuizCandidatAnswer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method QuizCandidatAnswer|null find($id, $lockMode = null, $lockVersion = null)
 * @method QuizCandidatAnswer|null findOneBy(array $criteria, array $orderBy = null)
 * @method QuizCandidatAnswer[]    findAll()
 * @method QuizCandidatAnswer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QuizCandidatAnswerRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, QuizCandidatAnswer::class);
    }

    // /**
    //  * @return QuizCandidatAnswer[] Returns an array of QuizCandidatAnswer objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('q.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?QuizCandidatAnswer
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
