<?php

namespace App\Repository;

use App\Entity\QuizSs;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method QuizSs|null find($id, $lockMode = null, $lockVersion = null)
 * @method QuizSs|null findOneBy(array $criteria, array $orderBy = null)
 * @method QuizSs[]    findAll()
 * @method QuizSs[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QuizSsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, QuizSs::class);
    }

    // /**
    //  * @return QuizSs[] Returns an array of QuizSs objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('q.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?QuizSs
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
