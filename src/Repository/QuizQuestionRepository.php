<?php
/**
 * Created by PhpStorm.
 * User: Livenexx
 * Date: 25/11/2019
 * Time: 15:25
 */

namespace App\Repository;


use App\Entity\QuizQuestion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class QuizQuestionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, QuizQuestion::class);
    }
}