<?php
/**
 * Created by PhpStorm.
 * User: Livenexx
 * Date: 25/11/2019
 * Time: 15:40
 */

namespace App\Repository;


use App\Entity\SsCandidat;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class SsCandidatRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SsCandidat::class);
    }
}