<?php

namespace App\Repository;

use App\Entity\Produit;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Produit|null find($id, $lockMode = null, $lockVersion = null)
 * @method Produit|null findOneBy(array $criteria, array $orderBy = null)
 * @method Produit[]    findAll()
 * @method Produit[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProduitRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Produit::class);
    }

    /**
     * @param $_chaine
     * @return Produit[]
     * @return mixed
     */
     public function findByProductDessignation($_chaine)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.designation like :val ')
            ->setParameter('val', '%'.$_chaine.'%')
            ->orderBy('p.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * this function paginate the list of the product
     * @param int $_current
     * @param int $_limit
     * @return Produit[]
     */
    public function paginateProdcut($_current=1,$_limit = 5){
         $_query    = $this->createQueryBuilder('p')
                        ->getQuery();
         $_paginate = $this->paginate($_query,$_current,$_limit);

         return $_paginate;
    }

    /**
     * @param $_dql
     * @param $_page
     * @param $_limit
     * @return Paginator
     */
    public function paginate($_dql,$_page ,$_limit)
    {
        $_paginator = new Paginator($_dql);
        $_paginator->setUseOutputWalkers(false);
        $_paginator->getQuery()
                   ->setFirstResult($_limit *($_page-1))
                   ->setMaxResults($_limit);

        return $_paginator;
    }
}
