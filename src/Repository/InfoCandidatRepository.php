<?php
/**
 * Created by PhpStorm.
 * User: Livenexx
 * Date: 25/11/2019
 * Time: 15:43
 */

namespace App\Repository;


use App\Entity\InfoCandidat;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class InfoCandidatRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InfoCandidat::class);
    }
}