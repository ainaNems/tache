<?php

namespace App\Repository;

use App\Entity\QuizCanidatResponse;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method QuizCanidatResponse|null find($id, $lockMode = null, $lockVersion = null)
 * @method QuizCanidatResponse|null findOneBy(array $criteria, array $orderBy = null)
 * @method QuizCanidatResponse[]    findAll()
 * @method QuizCanidatResponse[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QuizCanidatResponseRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, QuizCanidatResponse::class);
    }

    // /**
    //  * @return QuizCanidatResponse[] Returns an array of QuizCanidatResponse objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('q.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?QuizCanidatResponse
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
