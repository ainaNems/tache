<?php
/**
 * Created by PhpStorm.
 * User: Livenexx
 * Date: 25/11/2019
 * Time: 15:48
 */

namespace App\Repository;


use App\Entity\QuizType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

class QuizTypeRepository extends ServiceEntityRepository
{
    public function __construct(\Doctrine\Common\Persistence\ManagerRegistry $registry)
    {
        parent::__construct($registry, QuizType::class);
    }
}