<?php

namespace App\Form;

use App\Entity\Metier;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;

class MetierType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('metierName',TextType::class,['label'=>'Metier'])
            ->add('domain', EntityType::class, [
                'class'        => \App\Entity\Domain::class,
                'choice_label' => 'domainLabel',
                'label'        => 'Domain',
                'required'  => true,
                'placeholder'  => 'Domain'
            ])
            ->add('hardSkills', EntityType::class, [
                'class'        => \App\Entity\HardSkills::class,
                'choice_label' => 'hsName',
                'multiple' =>true,
                'label'        => 'Hard skills',
                'required'  => false,
                'attr'=> array("class"=>"select2"),
                'placeholder'  => 'HS'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Metier::class,
        ]);
    }
}
