<?php

namespace App\Form;

use App\Entity\Quiz;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class QuizType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('quizName')
            ->add('metiers', EntityType::class, [
                    'class'        => \App\Entity\Metier::class,
                    'choice_label' => 'metierName',
                    'label'        => 'Metier',
                    'required'  => false,
                    'placeholder'  => 'Metier(s)',
                    'multiple'  => true
                ])
            /*->add('softSkills', EntityType::class, [
                    'class'        => \App\Entity\SoftSkills::class,
                    'choice_label' => 'ssName',
                    'label'        => 'Soft skills',
                    'required'  => true,
                    'placeholder'  => 'Soft skills',
                    'multiple'  => true
                ])*/
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Quiz::class,
        ]);
    }
}
