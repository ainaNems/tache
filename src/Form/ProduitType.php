<?php

namespace App\Form;

use App\Entity\Produit;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProduitType extends AbstractType
{
    public function buildForm(FormBuilderInterface $_builder, array $options)
    {
        $_builder
            ->add('designation')
            ->add('quantite')
        ;
    }

    public function configureOptions(OptionsResolver $_resolver)
    {
        $_resolver->setDefaults([
            'data_class' => Produit::class,
        ]);
    }
}
