<?php

namespace App\Form;

use App\Entity\Utilisateur;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Validator\Constraints\File;
class UtilisateurType extends AbstractType
{
    public function buildForm(FormBuilderInterface $_builder, array $_options)
    {
        $_roles = array(
            'Utilisateur'        => 'ROLE_USER',
            'Administrateur'     => 'ROLE_ADMIN',
            'Super'              => 'ROLE_SUPER_ADMIN',
            'Candidat'              => 'ROLE_CANDIDAT',
        );
        $_builder
            ->add('username')
            ->add('email',EmailType::class)
            ->add('password',PasswordType::class)
            ->add('pics',FileType::class,[
                'label'=>'pics',
                'required'=>false,
                'mapped'=>false,
                'attr' => array(
                    'accept' => "image/jpeg, image/png"
                ),
                'constraints' => [
                    new File([
                        'maxSize' => '2M',
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/png',
                        ],
                        'mimeTypesMessage' => 'Please upload a JPG or PNG',
                    ])
                ]

            ])
            ->add('roles',ChoiceType::class,['choices'=>$_roles,
                                             'multiple' => true,
                                             'expanded' => false])
        ;
    }

    public function configureOptions(OptionsResolver $_resolver)
    {
        $_resolver->setDefaults([
            'data_class' => Utilisateur::class,
        ]);
    }
}
