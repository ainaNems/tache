<?php

namespace App\Form;

use App\Entity\QuizQuestionAnswer;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class QuizQuestionAnswerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('quizQuestion', EntityType::class, [
                'class'        => \App\Entity\QuizQuestion::class,
                'choice_label' => 'question',
                'label'        => 'Question',
                'required'  => true,
                'placeholder'  => 'Question'
            ])
            ->add('answer')
            ->add('score')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => QuizQuestionAnswer::class,
        ]);
    }
}
