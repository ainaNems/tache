<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\QuizSsRepository")
 */
class QuizSs
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $quiz_name;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Rule", inversedBy="quizSses")
     */
    private $rule;

    public function __construct()
    {
        $this->rule = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuizName(): ?string
    {
        return $this->quiz_name;
    }

    public function setQuizName(string $quiz_name): self
    {
        $this->quiz_name = $quiz_name;

        return $this;
    }

    /**
     * @return Collection|Rule[]
     */
    public function getRule(): Collection
    {
        return $this->rule;
    }

    public function addRule(Rule $rule): self
    {
        if (!$this->rule->contains($rule)) {
            $this->rule[] = $rule;
        }

        return $this;
    }

    public function removeRule(Rule $rule): self
    {
        if ($this->rule->contains($rule)) {
            $this->rule->removeElement($rule);
        }

        return $this;
    }
}
