<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SsCandidatRepository")
 */
class SsCandidat
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $ssLevel;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="ssCandidats")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    public function getId()
    {
        return $this->id;
    }

    public function getSsLevel()
    {
        return $this->ssLevel;
    }

    public function setSsLevel($ssLevel)
    {
        $this->ssLevel = $ssLevel;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
