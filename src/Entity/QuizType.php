<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\QuizTypeRepository")
 */
class QuizType
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $quizTypeName;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Quiz", mappedBy="quizType", orphanRemoval=true)
     */
    private $quizzes;

    public function __construct()
    {
        $this->quizzes = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getQuizTypeName()
    {
        return $this->quizTypeName;
    }

    public function setQuizTypeName($quizTypeName)
    {
        $this->quizTypeName = $quizTypeName;

        return $this;
    }

    /**
     * @return Collection|Quiz[]
     */
    public function getQuizzes(): Collection
    {
        return $this->quizzes;
    }

    public function addQuiz(Quiz $quiz): self
    {
        if (!$this->quizzes->contains($quiz)) {
            $this->quizzes[] = $quiz;
            $quiz->setQuizType($this);
        }

        return $this;
    }

    public function removeQuiz(Quiz $quiz): self
    {
        if ($this->quizzes->contains($quiz)) {
            $this->quizzes->removeElement($quiz);
            // set the owning side to null (unless already changed)
            if ($quiz->getQuizType() === $this) {
                $quiz->setQuizType(null);
            }
        }

        return $this;
    }
}
