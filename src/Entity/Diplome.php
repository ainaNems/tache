<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DiplomeRepository")
 */
class Diplome
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $diplomeUrl;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $diplomeDescription;

    public function getId()
    {
        return $this->id;
    }

    public function getDiplomeUrl()
    {
        return $this->diplomeUrl;
    }

    public function setDiplomeUrl($diplomeUrl)
    {
        $this->diplomeUrl = $diplomeUrl;

        return $this;
    }

    public function getDiplomeDescription()
    {
        return $this->diplomeDescription;
    }

    public function setDiplomeDescription($diplomeDescription)
    {
        $this->diplomeDescription = $diplomeDescription;

        return $this;
    }
}
