<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\QuizRepository")
 */
class Quiz
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $quizName;

    /**
     * @ORM\Column(type="time",nullable=true)
     */
    private $quizDuration;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\QuizType", inversedBy="quizzes")
     */
    private $quizType;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\QuizQuestion", mappedBy="quiz", orphanRemoval=true)
     */
    private $quizQuestions;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Metier", inversedBy="quizzes")
     */
    private $metiers;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\QuizCandidatAnswer", mappedBy="quiz")
     */
    private $quizCandidatAnswers;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Rule", inversedBy="quizzes")
     */
    private $rules;

    public function __construct()
    {
        $this->quizQuestions = new ArrayCollection();
        $this->metiers = new ArrayCollection();
        $this->quizCandidatAnswers = new ArrayCollection();
        $this->rules = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getQuizName()
    {
        return $this->quizName;
    }

    public function setQuizName($quizName)    {
        $this->quizName = $quizName;

        return $this;
    }

    public function getQuizDuration()
    {
        return $this->quizDuration;
    }

    public function setQuizDuration($quizDuration)
    {
        $this->quizDuration = $quizDuration;

        return $this;
    }

    public function getQuizCoeff()
    {
        return $this->quizCoeff;
    }

    public function setQuizCoeff($quizCoeff)
    {
        $this->quizCoeff = $quizCoeff;

        return $this;
    }

    public function getQuizType()
    {
        return $this->quizType;
    }

    public function setQuizType($quizType)
    {
        $this->quizType = $quizType;
    }

    /**
     * @return Collection|QuizQuestion[]
     */
    public function getQuizQuestions(): Collection
    {
        return $this->quizQuestions;
    }

    public function addQuizQuestion(QuizQuestion $quizQuestion): self
    {
        if (!$this->quizQuestions->contains($quizQuestion)) {
            $this->quizQuestions[] = $quizQuestion;
            $quizQuestion->setQuiz($this);
        }

        return $this;
    }

    public function removeQuizQuestion(QuizQuestion $quizQuestion): self
    {
        if ($this->quizQuestions->contains($quizQuestion)) {
            $this->quizQuestions->removeElement($quizQuestion);
            // set the owning side to null (unless already changed)
            if ($quizQuestion->getQuiz() === $this) {
                $quizQuestion->setQuiz(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Metier[]
     */
    public function getMetiers(): Collection
    {
        return $this->metiers;
    }

    public function addMetier(Metier $metier): self
    {
        if (!$this->metiers->contains($metier)) {
            $this->metiers[] = $metier;
        }

        return $this;
    }

    public function removeMetier(Metier $metier): self
    {
        if ($this->metiers->contains($metier)) {
            $this->metiers->removeElement($metier);
        }

        return $this;
    }

    /**
     * @return Collection|QuizCandidatAnswer[]
     */
    public function getQuizCandidatAnswers(): Collection
    {
        return $this->quizCandidatAnswers;
    }

    public function addQuizCandidatAnswer(QuizCandidatAnswer $quizCandidatAnswer): self
    {
        if (!$this->quizCandidatAnswers->contains($quizCandidatAnswer)) {
            $this->quizCandidatAnswers[] = $quizCandidatAnswer;
            $quizCandidatAnswer->setQuiz($this);
        }

        return $this;
    }

    public function removeQuizCandidatAnswer(QuizCandidatAnswer $quizCandidatAnswer): self
    {
        if ($this->quizCandidatAnswers->contains($quizCandidatAnswer)) {
            $this->quizCandidatAnswers->removeElement($quizCandidatAnswer);
            // set the owning side to null (unless already changed)
            if ($quizCandidatAnswer->getQuiz() === $this) {
                $quizCandidatAnswer->setQuiz(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Rule[]
     */
    public function getRules(): Collection
    {
        return $this->rules;
    }

    public function addRule(Rule $rule): self
    {
        if (!$this->rules->contains($rule)) {
            $this->rules[] = $rule;
        }

        return $this;
    }

    public function removeRule(Rule $rule): self
    {
        if ($this->rules->contains($rule)) {
            $this->rules->removeElement($rule);
        }

        return $this;
    }

}
