<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\QuizCandidatResponseRepository")
 */
class QuizCandidatResponse
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="time",nullable=true)
     */
    private $responseDuration;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\QuizQuestion", inversedBy="quizCandidatResponses")
     * @ORM\JoinColumn(nullable=false)
     */
    private $quizQuestion;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Utilisateur", inversedBy="quizCandidatResponses")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\QuizQuestionAnswer", inversedBy="quizCandidatResponses")
     * @ORM\JoinColumn(nullable=false)
     */
    private $quizResponse;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\QuizCandidatAnswer", inversedBy="quizCandidatResponses")
     */
    private $quizCandidatAnswer;

    public function getId()
    {
        return $this->id;
    }

    public function getResponseDuration()
    {
        return $this->responseDuration;
    }

    public function setResponseDuration( $responseDuration)
    {
        $this->responseDuration = $responseDuration;

        return $this;
    }

    public function getQuizQuestion()
    {
        return $this->quizQuestion;
    }

    public function setQuizQuestion(?quizQuestion $quizQuestion)
    {
        $this->quizQuestion = $quizQuestion;

        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser(?Utilisateur $user)
    {
        $this->user = $user;

        return $this;
    }

    public function getQuizResponse()
    {
        return $this->quizResponse;
    }

    public function setQuizResponse(?QuizQuestionAnswer $quizResponse)
    {
        $this->quizResponse = $quizResponse;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(?\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getQuizCandidatAnswer(): ?QuizCandidatAnswer
    {
        return $this->quizCandidatAnswer;
    }

    public function setQuizCandidatAnswer(?QuizCandidatAnswer $quizCandidatAnswer): self
    {
        $this->quizCandidatAnswer = $quizCandidatAnswer;

        return $this;
    }
}
