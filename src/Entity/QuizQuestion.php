<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\QuizQuestionRepository")
 */
class QuizQuestion
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Quiz", inversedBy="quizQuestions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $quiz;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $question;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\HardSkills", inversedBy="quizQuestions")
     */
    private $hardSkills;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\QuizQuestionAnswer", mappedBy="quizQuestion", orphanRemoval=true)
     */
    private $quizQuestionAnswers;

    public function __construct()
    {
        $this->quizCandidatResponses = new ArrayCollection();
        $this->quizQuestionAnswers = new ArrayCollection();
        $this->hardSkills = new ArrayCollection();
    }

    public function getQuiz()
    {
        return $this->quiz;
    }

    public function setQuiz( $quiz)
    {
        $this->quiz = $quiz;

        return $this;
    }

    public function getQuestion()
    {
        return $this->question;
    }

    public function setQuestion($question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Collection|HardSkills[]
     */
    public function getHardSkills(): Collection
    {
        return $this->hardSkills;
    }

    public function addHardSkill(HardSkills $hardSkill): self
    {
        if (!$this->hardSkills->contains($hardSkill)) {
            $this->hardSkills[] = $hardSkill;
        }

        return $this;
    }

    public function removeHardSkill(HardSkills $hardSkill): self
    {
        if ($this->hardSkills->contains($hardSkill)) {
            $this->hardSkills->removeElement($hardSkill);
        }

        return $this;
    }

    /**
     * @return Collection|QuizQuestionAnswers[]
     */
    public function getQuizQuestionAnswers(): Collection
    {
        return $this->quizQuestionAnswers;
    }

    public function addQuizQuestion(QuizQuestionAnswer $quizQuestionAnswer): self
    {
        if (!$this->quizQuestionAnswers->contains($quizQuestionAnswer)) {
            $this->quizQuestionAnswers[] = $quizQuestionAnswer;
            $quizQuestionAnswer->setQuiz($this);
        }

        return $this;
    }

    public function removeQuizQuestionAnswers(QuizQuestionAnswer $quizQuestionAnswer): self
    {
        if ($this->quizQuestionAnswers->contains($quizQuestionAnswer)) {
            $this->quizQuestionAnswers->removeElement($quizQuestionAnswer);
            // set the owning side to null (unless already changed)
            if ($quizQuestionAnswer->getQuiz() === $this) {
                $quizQuestionAnswer->setQuiz(null);
            }
        }

        return $this;
    }
}
