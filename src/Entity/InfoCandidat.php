<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\InfoCandidatRepository")
 */
class InfoCandidat
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $infoCandidatCvUrl;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $infoCandidatImageUrl;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HsCandidat", mappedBy="infoCandidat", orphanRemoval=true)
     */
    private $hardSkills;

    public function __construct()
    {
        $this->hardSkills = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getInfoCandidatCvUrl()
    {
        return $this->infoCandidatCvUrl;
    }

    public function setInfoCandidatCvUrl($infoCandidatCvUrl)
    {
        $this->infoCandidatCvUrl = $infoCandidatCvUrl;

        return $this;
    }

    public function getInfoCandidatImageUrl()
    {
        return $this->infoCandidatImageUrl;
    }

    public function setInfoCandidatImageUrl($infoCandidatImageUrl)
    {
        $this->infoCandidatImageUrl = $infoCandidatImageUrl;

        return $this;
    }

    /**
     * @return Collection|HsCandidat[]
     */
    public function getHardSkills(): Collection
    {
        return $this->hardSkills;
    }

    public function addHardSkill(HsCandidat $hardSkill): self
    {
        if (!$this->hardSkills->contains($hardSkill)) {
            $this->hardSkills[] = $hardSkill;
            $hardSkill->setInfoCandidat($this);
        }

        return $this;
    }

    public function removeHardSkill(HsCandidat $hardSkill): self
    {
        if ($this->hardSkills->contains($hardSkill)) {
            $this->hardSkills->removeElement($hardSkill);
            // set the owning side to null (unless already changed)
            if ($hardSkill->getInfoCandidat() === $this) {
                $hardSkill->setInfoCandidat(null);
            }
        }

        return $this;
    }
}
