<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CityRepository")
 */
class City
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=40)
     */
    private $cityName;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $cityPostalCode;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Country", inversedBy="cities")
     */
    private $country;

    public function getId()
    {
        return $this->id;
    }

    public function getCityName()
    {
        return $this->cityName;
    }

    public function setCityName($cityName)
    {
        $this->cityName = $cityName;

        return $this;
    }

    public function getCityPostalCode()
    {
        return $this->cityPostalCode;
    }

    public function setCityPostalCode($cityPostalCode)
    {
        $this->cityPostalCode = $cityPostalCode;

        return $this;
    }

     public function setId($id)
    {
        $this->id = $id;
    }

     public function getCountry()
     {
         return $this->country;
     }

     public function setCountry(?Country $country)
     {
         $this->country = $country;

         return $this;
     }
}
