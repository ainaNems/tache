<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MetierRepository")
 */
class Metier
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $metierName;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Domain", inversedBy="metiers")
     */
    private $domain;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Utilisateur", mappedBy="metier")
     */
    private $utilisateurs;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Quiz", mappedBy="metiers")
     */
    private $quizzes;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\HardSkills", inversedBy="metiers")
     */
    private $hardSkills;

    public function __construct()
    {
        $this->hardSkills = new ArrayCollection();
        $this->softSkills = new ArrayCollection();
        $this->utilisateurs = new ArrayCollection();
        $this->quizzes = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getMetierName()
    {
        return $this->metierName;
    }

    public function setMetierName($metierName)
    {
        $this->metierName = $metierName;

        return $this;
    }

    public function getDomain()
    {
        return $this->domain;
    }

    public function setDomain(?Domain $domain)
    {
        $this->domain = $domain;

        return $this;
    }

    /**
     * @return Collection|Utilisateur[]
     */
    public function getUtilisateurs(): Collection
    {
        return $this->utilisateurs;
    }

    public function addUtilisateur(Utilisateur $utilisateur): self
    {
        if (!$this->utilisateurs->contains($utilisateur)) {
            $this->utilisateurs[] = $utilisateur;
            $utilisateur->setMetier($this);
        }

        return $this;
    }

    public function removeUtilisateur(Utilisateur $utilisateur): self
    {
        if ($this->utilisateurs->contains($utilisateur)) {
            $this->utilisateurs->removeElement($utilisateur);
            // set the owning side to null (unless already changed)
            if ($utilisateur->getMetier() === $this) {
                $utilisateur->setMetier(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Quiz[]
     */
    public function getQuizzes(): Collection
    {
        return $this->quizzes;
    }

    public function addQuiz(Quiz $quiz): self
    {
        if (!$this->quizzes->contains($quiz)) {
            $this->quizzes[] = $quiz;
            $quiz->addMetier($this);
        }

        return $this;
    }

    public function removeQuiz(Quiz $quiz): self
    {
        if ($this->quizzes->contains($quiz)) {
            $this->quizzes->removeElement($quiz);
            $quiz->removeMetier($this);
        }

        return $this;
    }

    /**
     * @return Collection|HardSkills[]
     */
    public function getHardSkills(): Collection
    {
        return $this->hardSkills;
    }

    public function addHardSkill(HardSkills $hardSkill): self
    {
        if (!$this->hardSkills->contains($hardSkill)) {
            $this->hardSkills[] = $hardSkill;
        }

        return $this;
    }

    public function removeHardSkill(HardSkills $hardSkill): self
    {
        if ($this->hardSkills->contains($hardSkill)) {
            $this->hardSkills->removeElement($hardSkill);
        }

        return $this;
    }

}
