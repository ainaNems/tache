<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HsCandidatRepository")
 */
class HsCandidat
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $hsLevel;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\InfoCandidat", inversedBy="hardSkills")
     * @ORM\JoinColumn(nullable=false)
     */
    private $infoCandidat;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\HardSkills", inversedBy="hsCandidats")
     * @ORM\JoinColumn(nullable=false)
     */
    private $hardSkills;

    public function getId()
    {
        return $this->id;
    }

    public function getHsLevel()
    {
        return $this->hsLevel;
    }

    public function setHsLevel($hsLevel)
    {
        $this->hsLevel = $hsLevel;

        return $this;
    }

    public function getInfoCandidat(): ?InfoCandidat
    {
        return $this->infoCandidat;
    }

    public function setInfoCandidat(?InfoCandidat $infoCandidat): self
    {
        $this->infoCandidat = $infoCandidat;

        return $this;
    }

    public function getHardSkills(): ?HardSkills
    {
        return $this->hardSkills;
    }

    public function setHardSkills(?HardSkills $hardSkills): self
    {
        $this->hardSkills = $hardSkills;

        return $this;
    }
}
