<?php
/**
 * Created by PhpStorm.
 * User: Zo
 * Date: 07/08/2019
 * Time: 11:12
 */

namespace App\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="koann_user")
 */

class Utilisateur extends  BaseUser implements UserInterface
{
    /**
 * @ORM\Id
 * @ORM\Column(type="integer")
 * @ORM\GeneratedValue(strategy="AUTO")
 */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $picUrl;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $first_name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $facebook;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $twiter;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $linkedin;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imgUrl;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Domain", inversedBy="utilisateurs")
     */
    private $domain;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Metier", inversedBy="utilisateurs")
     */
    private $metier;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\QuizCandidatAnswer", mappedBy="candidat", orphanRemoval=true)
     */
    private $quizCandidatAnswers;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Sector", inversedBy="utilisateurs")
     */
    private $sector;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HardSkillsCandidat", mappedBy="candidat", orphanRemoval=true)
     */
    private $hardSkillsCandidats;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    private $telephone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $instagram;

    public function __construct()
    {
        parent::__construct();
        $this->quizCandidatAnswers = new ArrayCollection();
        $this->hardSkillsCandidats = new ArrayCollection();
        // your own logic
    }

    public function getPicUrl()
    {
        return $this->picUrl;
    }

    public function setPicUrl(?string $picUrl)
    {
        $this->picUrl = $picUrl;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName(string $name)
    {
        $this->name = $name;

        return $this;
    }

    public function getFirstName()
    {
        return $this->first_name;
    }

    public function setFirstName(?string $first_name)
    {
        $this->first_name = $first_name;

        return $this;
    }

    public function getFacebook(): ?string
    {
        return $this->facebook;
    }

    public function setFacebook(?string $facebook): self
    {
        $this->facebook = $facebook;

        return $this;
    }

    public function getTwiter(): ?string
    {
        return $this->twiter;
    }

    public function setTwiter(?string $twiter): self
    {
        $this->twiter = $twiter;

        return $this;
    }

    public function getLinkedin(): ?string
    {
        return $this->linkedin;
    }

    public function setLinkedin(?string $linkedin): self
    {
        $this->linkedin = $linkedin;

        return $this;
    }

    public function getImgUrl(): ?string
    {
        return $this->imgUrl;
    }

    public function setImgUrl(?string $imgUrl): self
    {
        $this->imgUrl = $imgUrl;

        return $this;
    }

    public function getDomain(): ?Domain
    {
        return $this->domain;
    }

    public function setDomain(?Domain $domain): self
    {
        $this->domain = $domain;

        return $this;
    }

    public function getMetier(): ?Metier
    {
        return $this->metier;
    }

    public function setMetier(?Metier $metier): self
    {
        $this->metier = $metier;

        return $this;
    }

    /**
     * @return Collection|QuizCandidatAnswer[]
     */
    public function getQuizCandidatAnswers(): Collection
    {
        return $this->quizCandidatAnswers;
    }

    public function addQuizCandidatAnswer(QuizCandidatAnswer $quizCandidatAnswer): self
    {
        if (!$this->quizCandidatAnswers->contains($quizCandidatAnswer)) {
            $this->quizCandidatAnswers[] = $quizCandidatAnswer;
            $quizCandidatAnswer->setCandidat($this);
        }

        return $this;
    }

    public function removeQuizCandidatAnswer(QuizCandidatAnswer $quizCandidatAnswer): self
    {
        if ($this->quizCandidatAnswers->contains($quizCandidatAnswer)) {
            $this->quizCandidatAnswers->removeElement($quizCandidatAnswer);
            // set the owning side to null (unless already changed)
            if ($quizCandidatAnswer->getCandidat() === $this) {
                $quizCandidatAnswer->setCandidat(null);
            }
        }

        return $this;
    }

    public function getSector(): ?Sector
    {
        return $this->sector;
    }

    public function setSector(?Sector $sector): self
    {
        $this->sector = $sector;

        return $this;
    }

    /**
     * @return Collection|HardSkillsCandidat[]
     */
    public function getHardSkillsCandidats(): Collection
    {
        return $this->hardSkillsCandidats;
    }

    public function addHardSkillsCandidat(HardSkillsCandidat $hardSkillsCandidat): self
    {
        if (!$this->hardSkillsCandidats->contains($hardSkillsCandidat)) {
            $this->hardSkillsCandidats[] = $hardSkillsCandidat;
            $hardSkillsCandidat->setCandidat($this);
        }

        return $this;
    }

    public function removeHardSkillsCandidat(HardSkillsCandidat $hardSkillsCandidat): self
    {
        if ($this->hardSkillsCandidats->contains($hardSkillsCandidat)) {
            $this->hardSkillsCandidats->removeElement($hardSkillsCandidat);
            // set the owning side to null (unless already changed)
            if ($hardSkillsCandidat->getCandidat() === $this) {
                $hardSkillsCandidat->setCandidat(null);
            }
        }

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(?string $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getInstagram(): ?string
    {
        return $this->instagram;
    }

    public function setInstagram(?string $instagram): self
    {
        $this->instagram = $instagram;

        return $this;
    }

}