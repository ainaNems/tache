<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\QuizCandidatAnswerRepository")
 */
class QuizCandidatAnswer
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="time",nullable=true)
     */
    private $duration;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Utilisateur", inversedBy="quizCandidatAnswers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $candidat;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Quiz", inversedBy="quizCandidatAnswers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $quiz;

    /**
     * @ORM\Column(type="float")
     */
    private $score;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\QuizCanidatResponse", mappedBy="quizCandidatAnswer")
     */
    private $quizCanidatResponses;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\QuizCandidatResponse", mappedBy="quizCandidatAnswer")
     */
    private $quizCandidatResponses;

    public function __construct()
    {
        $this->quizCanidatResponses = new ArrayCollection();
        $this->quizCandidatResponses = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDuration(): ?\DateTimeInterface
    {
        return $this->duration;
    }

    public function setDuration(\DateTimeInterface $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getCandidat(): ?Utilisateur
    {
        return $this->candidat;
    }

    public function setCandidat(?Utilisateur $candidat): self
    {
        $this->candidat = $candidat;

        return $this;
    }

    public function getQuiz(): ?Quiz
    {
        return $this->quiz;
    }

    public function setQuiz(?Quiz $quiz): self
    {
        $this->quiz = $quiz;

        return $this;
    }

    public function getScore(): ?int
    {
        return $this->score;
    }

    public function setScore($score): self
    {
        $this->score = $score;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(?\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return Collection|QuizCanidatResponse[]
     */
    public function getQuizCanidatResponses(): Collection
    {
        return $this->quizCanidatResponses;
    }

    public function addQuizCanidatResponse(QuizCanidatResponse $quizCanidatResponse): self
    {
        if (!$this->quizCanidatResponses->contains($quizCanidatResponse)) {
            $this->quizCanidatResponses[] = $quizCanidatResponse;
            $quizCanidatResponse->setQuizCandidatAnswer($this);
        }

        return $this;
    }

    public function removeQuizCanidatResponse(QuizCanidatResponse $quizCanidatResponse): self
    {
        if ($this->quizCanidatResponses->contains($quizCanidatResponse)) {
            $this->quizCanidatResponses->removeElement($quizCanidatResponse);
            // set the owning side to null (unless already changed)
            if ($quizCanidatResponse->getQuizCandidatAnswer() === $this) {
                $quizCanidatResponse->setQuizCandidatAnswer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|QuizCandidatResponse[]
     */
    public function getQuizCandidatResponses(): Collection
    {
        return $this->quizCandidatResponses;
    }

    public function addQuizCandidatResponse(QuizCandidatResponse $quizCandidatResponse): self
    {
        if (!$this->quizCandidatResponses->contains($quizCandidatResponse)) {
            $this->quizCandidatResponses[] = $quizCandidatResponse;
            $quizCandidatResponse->setQuizCandidatAnswer($this);
        }

        return $this;
    }

    public function removeQuizCandidatResponse(QuizCandidatResponse $quizCandidatResponse): self
    {
        if ($this->quizCandidatResponses->contains($quizCandidatResponse)) {
            $this->quizCandidatResponses->removeElement($quizCandidatResponse);
            // set the owning side to null (unless already changed)
            if ($quizCandidatResponse->getQuizCandidatAnswer() === $this) {
                $quizCandidatResponse->setQuizCandidatAnswer(null);
            }
        }

        return $this;
    }
}
