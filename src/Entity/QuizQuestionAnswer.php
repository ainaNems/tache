<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\QuizQuestionAnswerRepository")
 */
class QuizQuestionAnswer
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $answer;

    /**
     * @ORM\Column(type="integer")
     */
    private $score;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\QuizCandidatResponse", mappedBy="quizResponse", orphanRemoval=true)
     */
    private $quizCandidatResponses;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\QuizQuestion", inversedBy="quizQuestionAnswers")
     */
    private $quizQuestion;

    /**
     * @ORM\Column(type="integer",nullable=true)
     */
    private $answer_num;

    public function __construct()
    {
        $this->quizCandidatResponses = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAnswer()
    {
        return $this->answer;
    }

    public function setAnswer($answer)
    {
        $this->answer = $answer;

        return $this;
    }

    public function getScore()
    {
        return $this->score;
    }

    public function setScore($score)
    {
        $this->score = $score;

        return $this;
    }

    /**
     * @return Collection|QuizCandidatResponse[]
     */
    public function getQuizCandidatResponses(): Collection
    {
        return $this->quizCandidatResponses;
    }

    public function addQuizCandidatResponse(QuizCandidatResponse $quizCandidatResponse): self
    {
        if (!$this->quizCandidatResponses->contains($quizCandidatResponse)) {
            $this->quizCandidatResponses[] = $quizCandidatResponse;
            $quizCandidatResponse->setQuizResponse($this);
        }

        return $this;
    }

    public function removeQuizCandidatResponse(QuizCandidatResponse $quizCandidatResponse): self
    {
        if ($this->quizCandidatResponses->contains($quizCandidatResponse)) {
            $this->quizCandidatResponses->removeElement($quizCandidatResponse);
            // set the owning side to null (unless already changed)
            if ($quizCandidatResponse->getQuizResponse() === $this) {
                $quizCandidatResponse->setQuizResponse(null);
            }
        }

        return $this;
    }

    public function getQuizQuestion(): ?QuizQuestion
    {
        return $this->quizQuestion;
    }

    public function setQuizQuestion(?QuizQuestion $quizQuestion): self
    {
        $this->quizQuestion = $quizQuestion;

        return $this;
    }

    public function getAnswerNum(): ?int
    {
        return $this->answer_num;
    }

    public function setAnswerNum(int $answer_num): self
    {
        $this->answer_num = $answer_num;

        return $this;
    }
}
