<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CivilityRepository")
 */
class Civility
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $civilityName;

    public function getId()
    {
        return $this->id;
    }

    public function getCivilityName()
    {
        return $this->civilityName;
    }

    public function setCivilityName($civilityName)
    {
        $this->civilityName = $civilityName;

        return $this;
    }
}
