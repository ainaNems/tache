<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DomainRepository")
 */
class Domain
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer",nullable=true)
     */
    private $domainCode;

    /**
     * @ORM\Column(type="string", length=40)
     */
    private $domainLabel;

    /**
     * @ORM\Column(type="string", length=20,nullable=true)
     */
    private $domainConceptType;

    /**
     * @ORM\Column(type="text",nullable=true)
     */
    private $domainDesc;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Sector", inversedBy="domains")
     */
    private $sector;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Metier", mappedBy="domain")
     */
    private $metiers;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Utilisateur", mappedBy="domain")
     */
    private $utilisateurs;

    public function __construct()
    {
        $this->metiers = new ArrayCollection();
        $this->utilisateurs = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getDomainCode()
    {
        return $this->domainCode;
    }

    public function setDomainCode($domainCode)
    {
        $this->domainCode = $domainCode;

        return $this;
    }

    public function getDomainLabel()
    {
        return $this->domainLabel;
    }

    public function setDomainLabel($domainLabel)
    {
        $this->domainLabel = $domainLabel;

        return $this;
    }

    public function getDomainConceptType()
    {
        return $this->domainConceptType;
    }

    public function setDomainConceptType($domainConceptType)
    {
        $this->domainConceptType = $domainConceptType;

        return $this;
    }

    public function getDomainDesc()
    {
        return $this->domainDesc;
    }

    public function setDomainDesc($domainDesc)
    {
        $this->domainDesc = $domainDesc;

        return $this;
    }

    public function getSector()
    {
        return $this->sector;
    }

    public function setSector(?Sector $sector)
    {
        $this->sector = $sector;

        return $this;
    }

    /**
     * @return Collection|Metier[]
     */
    public function getMetiers()
    {
        return $this->metiers;
    }

    public function addMetier(Metier $metier)
    {
        if (!$this->metiers->contains($metier)) {
            $this->metiers[] = $metier;
            $metier->setDomain($this);
        }

        return $this;
    }

    public function removeMetier(Metier $metier)
    {
        if ($this->metiers->contains($metier)) {
            $this->metiers->removeElement($metier);
            // set the owning side to null (unless already changed)
            if ($metier->getDomain() === $this) {
                $metier->setDomain(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Utilisateur[]
     */
    public function getUtilisateurs(): Collection
    {
        return $this->utilisateurs;
    }

    public function addUtilisateur(Utilisateur $utilisateur): self
    {
        if (!$this->utilisateurs->contains($utilisateur)) {
            $this->utilisateurs[] = $utilisateur;
            $utilisateur->setDomain($this);
        }

        return $this;
    }

    public function removeUtilisateur(Utilisateur $utilisateur): self
    {
        if ($this->utilisateurs->contains($utilisateur)) {
            $this->utilisateurs->removeElement($utilisateur);
            // set the owning side to null (unless already changed)
            if ($utilisateur->getDomain() === $this) {
                $utilisateur->setDomain(null);
            }
        }

        return $this;
    }

}
