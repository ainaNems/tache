<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\QuizCanidatResponseRepository")
 */
class QuizCanidatResponse
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\QuizCandidatAnswer", inversedBy="quizCanidatResponses")
     */
    private $quizCandidatAnswer;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuizCandidatAnswer(): ?QuizCandidatAnswer
    {
        return $this->quizCandidatAnswer;
    }

    public function setQuizCandidatAnswer(?QuizCandidatAnswer $quizCandidatAnswer): self
    {
        $this->quizCandidatAnswer = $quizCandidatAnswer;

        return $this;
    }
}
