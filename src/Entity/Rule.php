<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RuleRepository")
 */
class Rule
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ruleDescription;

    /**
     * @ORM\Column(type="integer")
     */
    private $maxAnswerId;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\SoftSkills", inversedBy="rules")
     */
    private $softSkills;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\QuizSs", mappedBy="rule")
     */
    private $quizSses;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Quiz", mappedBy="rules")
     */
    private $quizzes;

    public function __construct()
    {
        $this->softSkills = new ArrayCollection();
        $this->quizSses = new ArrayCollection();
        $this->quizzes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRuleDescription(): ?string
    {
        return $this->ruleDescription;
    }

    public function setRuleDescription(string $ruleDescription): self
    {
        $this->ruleDescription = $ruleDescription;

        return $this;
    }

    public function getMaxAnswerId(): ?int
    {
        return $this->maxAnswerId;
    }

    public function setMaxAnswerId(int $maxAnswerId): self
    {
        $this->maxAnswerId = $maxAnswerId;

        return $this;
    }

    /**
     * @return Collection|SoftSkills[]
     */
    public function getSoftSkills(): Collection
    {
        return $this->softSkills;
    }

    public function addSoftSkill(SoftSkills $softSkill): self
    {
        if (!$this->softSkills->contains($softSkill)) {
            $this->softSkills[] = $softSkill;
        }

        return $this;
    }

    public function removeSoftSkill(SoftSkills $softSkill): self
    {
        if ($this->softSkills->contains($softSkill)) {
            $this->softSkills->removeElement($softSkill);
        }

        return $this;
    }

    public function hasSoftSkills(SoftSkills $softSkill){
        if ($this->softSkills->contains($softSkill)) {
            return true;
        }

        return false;
    }

    /**
     * @return Collection|QuizSs[]
     */
    public function getQuizSses(): Collection
    {
        return $this->quizSses;
    }

    public function addQuizSs(QuizSs $quizSs): self
    {
        if (!$this->quizSses->contains($quizSs)) {
            $this->quizSses[] = $quizSs;
            $quizSs->addRule($this);
        }

        return $this;
    }

    public function removeQuizSs(QuizSs $quizSs): self
    {
        if ($this->quizSses->contains($quizSs)) {
            $this->quizSses->removeElement($quizSs);
            $quizSs->removeRule($this);
        }

        return $this;
    }

    /**
     * @return Collection|Quiz[]
     */
    public function getQuizzes(): Collection
    {
        return $this->quizzes;
    }

    public function addQuiz(Quiz $quiz): self
    {
        if (!$this->quizzes->contains($quiz)) {
            $this->quizzes[] = $quiz;
            $quiz->addRule($this);
        }

        return $this;
    }

    public function removeQuiz(Quiz $quiz): self
    {
        if ($this->quizzes->contains($quiz)) {
            $this->quizzes->removeElement($quiz);
            $quiz->removeRule($this);
        }

        return $this;
    }
}
