<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SoftSkillsRepository")
 */
class SoftSkills
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $ssName;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Rule", mappedBy="softSkills")
     */
    private $rules;

    public function __construct()
    {
        $this->metier = new ArrayCollection();
        $this->rules = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getSsName()
    {
        return $this->ssName;
    }

    public function setSsName($ssName)
    {
        $this->ssName = $ssName;

        return $this;
    }

    /**
     * @return Collection|Rule[]
     */
    public function getRules(): Collection
    {
        return $this->rules;
    }

    public function addRule(Rule $rule): self
    {
        if (!$this->rules->contains($rule)) {
            $this->rules[] = $rule;
            $rule->addSoftSkill($this);
        }

        return $this;
    }

    public function removeRule(Rule $rule): self
    {
        if ($this->rules->contains($rule)) {
            $this->rules->removeElement($rule);
            $rule->removeSoftSkill($this);
        }

        return $this;
    }

}
