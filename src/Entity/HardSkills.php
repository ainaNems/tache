<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HardSkillsRepository")
 */
class HardSkills
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $hsName;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HsCandidat", mappedBy="hardSkills", orphanRemoval=true)
     */
    private $hsCandidats;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Metier", mappedBy="hardSkills")
     */
    private $metiers;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\QuizQuestion", mappedBy="hardSkills")
     */
    private $quizQuestions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HardSkillsCandidat", mappedBy="hardSkill", orphanRemoval=true)
     */
    private $hardSkillsCandidats;

    public function __construct()
    {
        $this->hsCandidats = new ArrayCollection();
        $this->metier = new ArrayCollection();
        $this->quizzes = new ArrayCollection();
        $this->metiers = new ArrayCollection();
        $this->quizQuestions = new ArrayCollection();
        $this->hardSkillsCandidats = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getHsName()
    {
        return $this->hsName;
    }

    public function setHsName($hsName)
    {
        $this->hsName = $hsName;

        return $this;
    }

    /**
     * @return Collection|HsCandidat[]
     */
    public function getHsCandidats()
    {
        return $this->hsCandidats;
    }

    public function addHsCandidat(HsCandidat $hsCandidat): self
    {
        if (!$this->hsCandidats->contains($hsCandidat)) {
            $this->hsCandidats[] = $hsCandidat;
            $hsCandidat->setHardSkills($this);
        }

        return $this;
    }

    public function removeHsCandidat(HsCandidat $hsCandidat): self
    {
        if ($this->hsCandidats->contains($hsCandidat)) {
            $this->hsCandidats->removeElement($hsCandidat);
            // set the owning side to null (unless already changed)
            if ($hsCandidat->getHardSkills() === $this) {
                $hsCandidat->setHardSkills(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Metier[]
     */
    public function getMetiers(): Collection
    {
        return $this->metiers;
    }

    public function addMetier(Metier $metier): self
    {
        if (!$this->metiers->contains($metier)) {
            $this->metiers[] = $metier;
            $metier->addHardSkill($this);
        }

        return $this;
    }

    public function removeMetier(Metier $metier): self
    {
        if ($this->metiers->contains($metier)) {
            $this->metiers->removeElement($metier);
            $metier->removeHardSkill($this);
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->hsName;
    }

    /**
     * @return Collection|QuizQuestion[]
     */
    public function getQuizQuestions(): Collection
    {
        return $this->quizQuestions;
    }

    public function addQuizQuestion(QuizQuestion $quizQuestion): self
    {
        if (!$this->quizQuestions->contains($quizQuestion)) {
            $this->quizQuestions[] = $quizQuestion;
            $quizQuestion->addHardSkill($this);
        }

        return $this;
    }

    public function removeQuizQuestion(QuizQuestion $quizQuestion): self
    {
        if ($this->quizQuestions->contains($quizQuestion)) {
            $this->quizQuestions->removeElement($quizQuestion);
            $quizQuestion->removeHardSkill($this);
        }

        return $this;
    }

    /**
     * @return Collection|HardSkillsCandidat[]
     */
    public function getHardSkillsCandidats(): Collection
    {
        return $this->hardSkillsCandidats;
    }

    public function addHardSkillsCandidat(HardSkillsCandidat $hardSkillsCandidat): self
    {
        if (!$this->hardSkillsCandidats->contains($hardSkillsCandidat)) {
            $this->hardSkillsCandidats[] = $hardSkillsCandidat;
            $hardSkillsCandidat->setHardSkill($this);
        }

        return $this;
    }

    public function removeHardSkillsCandidat(HardSkillsCandidat $hardSkillsCandidat): self
    {
        if ($this->hardSkillsCandidats->contains($hardSkillsCandidat)) {
            $this->hardSkillsCandidats->removeElement($hardSkillsCandidat);
            // set the owning side to null (unless already changed)
            if ($hardSkillsCandidat->getHardSkill() === $this) {
                $hardSkillsCandidat->setHardSkill(null);
            }
        }

        return $this;
    }
}
