$("#id-pics").hide();
$("#id-test-show").click(function () {
    $("#id-test").show();
});
$("#utilisateur_pics").change(function (event) {
    var reader = new FileReader();
    reader.onload = function()
    {
        var output = document.getElementById('id-output_image');
        output.src = reader.result;
        $("#id-pics").show();
    }
    reader.readAsDataURL(event.target.files[0]);
})
