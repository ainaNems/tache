$(document).ready(function(){
    $(".kl-message").hide();
    var current = 1;
    function cancel()
    {
        $(".kl-update").hide();
        $(".kl-save").show();
        $("#id-produit").val(0);
        $("#produit_designation").val("");
        $("#produit_quantite").val("");
    }
    cancel();
    function paginate(page)
    {
        var output = '<li class="page-item"><a class="page-link kl-previous " href="#">Previous</a></li>'
        for(var i = 1 ; i <= page ; i++)
        {
            output += '<li class="page-item"><a class="page-link kl-get-page" href="#" num_page="'+i +'">'+i+'</a></li>'
        }
        output += '<li class="page-item"><a class="page-link kl-next" href="#">Next</a></li>'
        $(".kl-page").html(output);
        $(".kl-page li").removeClass('active');
        if(current == 1)
        {
            $(".kl-previous").parent().addClass('disabled');
        }
        else
        {
            $(".kl-previous").parent().removeClass('disabled');
        }
        if(current == page)
        {
            $(".kl-next").parent().addClass('disabled');
        }
        else
        {
            $(".kl-next").parent().removeClass('disabled');
        }
        $("[num_page='"+current+"']").parent().addClass('active');
        $('.kl-get-page').click(function () {
            current = $(this).attr('num_page');
            getAllProducts();
        });
        $('.kl-previous').click(function () {
           current--;
           getAllProducts();
        });
        $('.kl-next').click(function () {
            current++;
            getAllProducts();
        })
    }
    function getAllProducts()
    {
        $.ajax({
            method   : 'GET',
            datatype : 'json',
            data     : {page : current},
            url      : url.get_all_product,
            success  : function(data){
                var number_of_page = Math.ceil(data.number/5)
                paginate(number_of_page)
                repopulateTable(data)
                select_produit()
                delete_produit()
            }
        });
    }
    getAllProducts();
    function repopulateTable(data)
    {
        var outpout = "<tr><th>Id<th> Designation<th> Quantite<th>action</tr>";
        var number_of_produit = data.produits.length;
        for (var i = 0 ; i < number_of_produit ; i++)
        {
            outpout += "<tr><td>"+data.produits[i].id+"<td>"+data.produits[i].designation+"<td>"+data.produits[i].quantite;
            outpout += "<td><button class='btn btn-dark kl-select' id_attr='"+ data.produits[i].id+"'>edit</button> ";
            outpout += "  <button class='btn btn-danger kl-delete' id_attr='"+ data.produits[i].id +"'>delete</button> </tr>";
        }
        $("#id-liste").html(outpout) ;
    }
    $(".kl-filtre-produit").keyup(function(){
        var filtre = $(this).val();
        $.ajax({
            url      : url.filter_product,
            method   : 'get',
            datatype : 'json',
            data     : { filtre : filtre },
            success  : function (data){
                repopulateTable(data);
            }
        })
    });
    $(".kl-save").click(function () {
        var designation = $('#produit_designation').val();
        var quantite    = $('#produit_quantite').val();
        $.ajax({
            method: 'post',
            data: {
                designation: designation,
                quantite   : quantite,
            },
            url: url.save_product,
            datatype: 'json',
            success:function(data){
                cancel();
                getAllProducts();
                $(".kl-message").html(data.message);
                $(".kl-message").show().fadeOut(5000);
            },
            error:function(){
                console.log('ko');
            }
        });
    });
    function select_produit(){
        $(".kl-select").click(function(){
            var produit_id = $(this).attr('id_attr');
            $.ajax({
                method  : 'get',
                datatype: 'json',
                data    : {id : produit_id},
                url     : url.get_one_product,
                success : function(data){
                    $('#produit_designation').val(data.produit.designation);
                    $('#produit_quantite').val(data.produit.quantite);
                    $('#id-produit').val(data.produit.id);
                    $('.kl-update').show();
                    $('.kl-save').hide();
                },
                error   : function(){
                    console.log('ko')
                }
            });
        });
    }
    function delete_produit(){
        $(".kl-delete").click(function(){
            var produit_id = $(this).attr('id_attr');
            if (confirm("voulez vous vraiment suppprimer ce produit?"))
            {
                $.ajax({
                    method  : 'post',
                    datatype: 'json',
                    data    : {id : produit_id},
                    url     : url.delete_product,
                    success : function(data){
                        getAllProducts();
                        $(".kl-message").html(data.message);
                        $(".kl-message").show().fadeOut(5000);
                    },
                    error   : function(){
                        console.log('ko')
                    }
                });
            }
        });
    }
    $(".kl-update").click(function () {
        var designation = $('#produit_designation').val();
        var quantite    = $('#produit_quantite').val();
        var id          = $('#id-produit').val();
        console.log(id);
        $.ajax({
            method: 'post',
            data: {
                designation: designation,
                quantite   : quantite,
                id         : id,
            },
            url: url.update_product,
            datatype: 'json',
            success:function(data){
                cancel();
                getAllProducts();
                $(".kl-message").html(data.message);
                $(".kl-message").show().fadeOut(5000);
            },
            error:function(){
                console.log('ko');
            }
        });
    });
    $(".kl-cancel").click(function(){
        cancel();
    });
});
