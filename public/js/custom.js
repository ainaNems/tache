$(document).ready(function(){
	$(".evolution").on('click',function(){
		var title = $(this).data("title");
		$.ajax({
			url : evolution_url,
			data : { user : $(this).data("user"), skill : $(this).data("skill") },
			method : "POST",
			dataType : 'JSON',
			success: function(response){
				var evolution = document.getElementById('chart-evolution');
				var labels = [];
				var data = [];
				var length = response.length;
				for(var i = 0; i < length ; i++){
					data.push(response[i]["score"]*10);
					var date = (((response[i]["date"]["date"]).split(" "))[0]).split("-");
					    label = date[2]+"/"+date[1]+"/"+date[0];
					labels.push(label);
				}
		        var jobChart = new Chart(evolution, {
		            type: 'line',
		            data: {
		                labels: labels,
		                datasets: [{
		                    label: title,
		                    data: data,
		                    backgroundColor:"rgba(28,155,224,0.5)",
		                    borderColor: "'rgba(180,250,120,1)",
		                    borderWidth: 1,
		                    fill : true
		                }]
		            },
		            options: {
		                scales: {
			                    yAxes: [{
	                				ticks: {
	                    					fontStyle: "bold",
	                    					beginAtZero: true,
	                    					padding: 20
	                					},
	                				gridLines: {
	                    				drawTicks: false,
	                    				display: false,
	                    				zeroLineColor: "transparent"
	                				}
								}],
								 xAxes: [{
                							gridLines: {
                    							zeroLineColor: "transparent",
                    							display: false
											},
                							ticks: {
                   								 padding: 20,
                    						     fontStyle: "bold"
                							}
           								 }]
		                	}
		            }
		        });
			}
		})
	});
});