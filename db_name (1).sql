-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  lun. 13 jan. 2020 à 11:27
-- Version du serveur :  5.7.26
-- Version de PHP :  7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `db_name`
--

-- --------------------------------------------------------

--
-- Structure de la table `candidat`
--

DROP TABLE IF EXISTS `candidat`;
CREATE TABLE IF NOT EXISTS `candidat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_visible` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `candidat_quiz`
--

DROP TABLE IF EXISTS `candidat_quiz`;
CREATE TABLE IF NOT EXISTS `candidat_quiz` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `duration` time NOT NULL,
  `score` int(11) NOT NULL,
  `maxscore` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `city`
--

DROP TABLE IF EXISTS `city`;
CREATE TABLE IF NOT EXISTS `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) DEFAULT NULL,
  `city_name` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city_postal_code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_2D5B0234F92F3E70` (`country_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `city`
--

INSERT INTO `city` (`id`, `country_id`, `city_name`, `city_postal_code`) VALUES
(1, 1, 'Antananarivo', '101'),
(2, 1, 'Fianarantsoa', '301'),
(3, 1, 'Ambatolampy', '104');

-- --------------------------------------------------------

--
-- Structure de la table `civility`
--

DROP TABLE IF EXISTS `civility`;
CREATE TABLE IF NOT EXISTS `civility` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `civility_name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `country`
--

DROP TABLE IF EXISTS `country`;
CREATE TABLE IF NOT EXISTS `country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `country`
--

INSERT INTO `country` (`id`, `country_name`) VALUES
(1, 'Madagascar'),
(2, 'France');

-- --------------------------------------------------------

--
-- Structure de la table `diplome`
--

DROP TABLE IF EXISTS `diplome`;
CREATE TABLE IF NOT EXISTS `diplome` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `diplome_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `diplome_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `domain`
--

DROP TABLE IF EXISTS `domain`;
CREATE TABLE IF NOT EXISTS `domain` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sector_id` int(11) DEFAULT NULL,
  `domain_code` int(11) NOT NULL,
  `domain_label` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `domain_concept_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `domain_desc` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_A7A91E0BDE95C867` (`sector_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `domain`
--

INSERT INTO `domain` (`id`, `sector_id`, `domain_code`, `domain_label`, `domain_concept_type`, `domain_desc`) VALUES
(1, 1, 101, 'Informatique', '1580', 'info'),
(2, 1, 10, 'Marketing', '1580', 'marché');

-- --------------------------------------------------------

--
-- Structure de la table `hard_skills`
--

DROP TABLE IF EXISTS `hard_skills`;
CREATE TABLE IF NOT EXISTS `hard_skills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hs_name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `hard_skills`
--

INSERT INTO `hard_skills` (`id`, `hs_name`) VALUES
(1, 'Php'),
(2, 'Langage c'),
(3, 'Angular'),
(4, 'Comptabilité');

-- --------------------------------------------------------

--
-- Structure de la table `hs_candidat`
--

DROP TABLE IF EXISTS `hs_candidat`;
CREATE TABLE IF NOT EXISTS `hs_candidat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `info_candidat_id` int(11) NOT NULL,
  `hard_skills_id` int(11) NOT NULL,
  `hs_level` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_3E921DB5D7E8C17D` (`info_candidat_id`),
  KEY `IDX_3E921DB544C6C0E1` (`hard_skills_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `info_candidat`
--

DROP TABLE IF EXISTS `info_candidat`;
CREATE TABLE IF NOT EXISTS `info_candidat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `info_candidat_cv_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `info_candidat_image_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `koann_user`
--

DROP TABLE IF EXISTS `koann_user`;
CREATE TABLE IF NOT EXISTS `koann_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `pic_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twiter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `domain_id` int(11) DEFAULT NULL,
  `metier_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_AAF5717C92FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_AAF5717CA0D96FBF` (`email_canonical`),
  UNIQUE KEY `UNIQ_AAF5717CC05FB297` (`confirmation_token`),
  KEY `IDX_AAF5717C115F0EE5` (`domain_id`),
  KEY `IDX_AAF5717CED16FA20` (`metier_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `koann_user`
--

INSERT INTO `koann_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `confirmation_token`, `password_requested_at`, `roles`, `pic_url`, `name`, `first_name`, `facebook`, `twiter`, `linkedin`, `img_url`, `domain_id`, `metier_id`) VALUES
(1, 'kotoarindrasana', 'kotoarindrasana', 'leong@gmail.com', 'leong@gmail.com', 1, NULL, 'bemasoandrokely', '2019-12-03 13:24:07', NULL, NULL, 'a:0:{}', 'telechargement2-5de66274ac34b.jpeg', 'rakoto', 'rabezanozano', NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'kotoarindrasanas', 'kotoarindrasanas', 'leongy@gmail.com', 'leongy@gmail.com', 1, NULL, 'bemasoandrokely', '2019-12-03 13:29:53', NULL, NULL, 'a:1:{i:0;s:13:\"ROLE_CANDIDAT\";}', 'hqdefault-5de66389d0223.jpeg', 'rakoto', 'rabezanozano', NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'bema kely', 'bema kely', 'nems@gmail.com', 'nems@gmail.com', 0, NULL, '$2y$13$lcpFUMsU/y70exuaHIZ0FesEjwX27RN572G5KMkhDUlsUAPHh1I52', NULL, NULL, NULL, 'a:0:{}', NULL, 'bema', 'soandro', NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'nemsaina', 'nemsaina', 'nems@aina.com', 'nems@aina.com', 1, NULL, '$2y$13$.X5JyGER0nXpnrAwxQLLYe4ANkHXMtryZq34rNV49Bzi/t9zPBw6C', '2019-12-04 06:26:15', NULL, NULL, 'a:0:{}', NULL, 'nems', 'aina', NULL, NULL, NULL, NULL, NULL, NULL),
(6, 'nemsaina1', 'nemsaina1', 'nemsa@aina.com', 'nemsa@aina.com', 1, NULL, '$2y$13$inPS7Jp0mESnEUM5znteL.qOvMPleSbfLSEjZTUS1Cuk.QMEkD0Ca', '2019-12-17 13:40:07', NULL, NULL, 'a:1:{i:0;s:8:\"CANDIDAT\";}', 'photographiewejune-5df8dec549f00.jpeg', 'nems', 'aina', NULL, NULL, NULL, NULL, 1, 1),
(7, 'nemsaina32', 'nemsaina32', 'nehemia@randr.com', 'nehemia@randr.com', 1, NULL, '$2y$13$RTxo5HrABK/LNUkChAWE6u0WC7WtZk3.aFm.RP29ObpQVFLbX.rSG', '2019-12-19 04:14:11', NULL, NULL, 'a:1:{i:0;s:8:\"CANDIDAT\";}', 'telechargement2-5df3a27e17865.jpeg', 'Randrianantenaina', 'Sitraka Nehemia  Léon', NULL, NULL, NULL, NULL, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `metier`
--

DROP TABLE IF EXISTS `metier`;
CREATE TABLE IF NOT EXISTS `metier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain_id` int(11) DEFAULT NULL,
  `metier_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_51A00D8C115F0EE5` (`domain_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `metier`
--

INSERT INTO `metier` (`id`, `domain_id`, `metier_name`) VALUES
(1, 1, 'Developpeur web'),
(2, 1, 'Administrateur System'),
(3, 2, 'Comptable'),
(4, 1, 'Developpeur');

-- --------------------------------------------------------

--
-- Structure de la table `metier_hard_skills`
--

DROP TABLE IF EXISTS `metier_hard_skills`;
CREATE TABLE IF NOT EXISTS `metier_hard_skills` (
  `metier_id` int(11) NOT NULL,
  `hard_skills_id` int(11) NOT NULL,
  PRIMARY KEY (`metier_id`,`hard_skills_id`),
  KEY `IDX_B8BA403CED16FA20` (`metier_id`),
  KEY `IDX_B8BA403C44C6C0E1` (`hard_skills_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `metier_hard_skills`
--

INSERT INTO `metier_hard_skills` (`metier_id`, `hard_skills_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(2, 1),
(2, 2),
(4, 1),
(4, 2),
(4, 3);

-- --------------------------------------------------------

--
-- Structure de la table `migration_versions`
--

DROP TABLE IF EXISTS `migration_versions`;
CREATE TABLE IF NOT EXISTS `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `migration_versions`
--

INSERT INTO `migration_versions` (`version`, `executed_at`) VALUES
('20191203123927', '2019-12-03 12:40:13'),
('20191203131244', '2019-12-03 13:14:01'),
('20191212084250', '2019-12-12 08:43:14'),
('20191213055325', '2019-12-13 06:00:00'),
('20191213060612', '2019-12-13 07:15:54'),
('20191213080848', '2019-12-13 08:08:58'),
('20191213081957', '2019-12-13 08:20:08'),
('20191213083629', '2019-12-13 08:36:38'),
('20191213084557', '2019-12-13 08:46:09'),
('20191213111244', '2019-12-13 11:12:54'),
('20191217073657', '2019-12-17 07:37:17'),
('20191217122309', '2019-12-17 12:23:32'),
('20191217125550', '2019-12-17 12:56:29');

-- --------------------------------------------------------

--
-- Structure de la table `produit`
--

DROP TABLE IF EXISTS `produit`;
CREATE TABLE IF NOT EXISTS `produit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `designation` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantite` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `quiz`
--

DROP TABLE IF EXISTS `quiz`;
CREATE TABLE IF NOT EXISTS `quiz` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quiz_type_id` int(11) DEFAULT NULL,
  `quiz_name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quiz_duration` time NOT NULL,
  `quiz_coeff` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_A412FA92D7162133` (`quiz_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `quiz`
--

INSERT INTO `quiz` (`id`, `quiz_type_id`, `quiz_name`, `quiz_duration`, `quiz_coeff`) VALUES
(1, 1, 'Programmation', '00:05:00', 5),
(2, 2, 'Algorithme', '00:10:00', 15),
(3, 2, 'Test', '00:05:00', 2),
(4, 2, 'Puzzle', '00:01:00', 15);

-- --------------------------------------------------------

--
-- Structure de la table `quiz_candidat_answer`
--

DROP TABLE IF EXISTS `quiz_candidat_answer`;
CREATE TABLE IF NOT EXISTS `quiz_candidat_answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `candidat_id` int(11) NOT NULL,
  `quiz_id` int(11) NOT NULL,
  `duration` time DEFAULT NULL,
  `score` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_5F120D088D0EB82` (`candidat_id`),
  KEY `IDX_5F120D08853CD175` (`quiz_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `quiz_candidat_answer`
--

INSERT INTO `quiz_candidat_answer` (`id`, `candidat_id`, `quiz_id`, `duration`, `score`) VALUES
(3, 7, 1, NULL, 35),
(4, 7, 1, NULL, 35),
(5, 7, 2, NULL, 15);

-- --------------------------------------------------------

--
-- Structure de la table `quiz_candidat_response`
--

DROP TABLE IF EXISTS `quiz_candidat_response`;
CREATE TABLE IF NOT EXISTS `quiz_candidat_response` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quiz_question_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `quiz_response_id` int(11) NOT NULL,
  `response_duration` time DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_F6E900E83101E51F` (`quiz_question_id`),
  KEY `IDX_F6E900E8A76ED395` (`user_id`),
  KEY `IDX_F6E900E8D4D53BE0` (`quiz_response_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `quiz_candidat_response`
--

INSERT INTO `quiz_candidat_response` (`id`, `quiz_question_id`, `user_id`, `quiz_response_id`, `response_duration`) VALUES
(3, 1, 7, 1, NULL),
(4, 2, 7, 4, NULL),
(5, 1, 7, 1, NULL),
(6, 2, 7, 4, NULL),
(7, 3, 7, 7, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `quiz_metier`
--

DROP TABLE IF EXISTS `quiz_metier`;
CREATE TABLE IF NOT EXISTS `quiz_metier` (
  `quiz_id` int(11) NOT NULL,
  `metier_id` int(11) NOT NULL,
  PRIMARY KEY (`quiz_id`,`metier_id`),
  KEY `IDX_BCE4FDD5853CD175` (`quiz_id`),
  KEY `IDX_BCE4FDD5ED16FA20` (`metier_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `quiz_metier`
--

INSERT INTO `quiz_metier` (`quiz_id`, `metier_id`) VALUES
(1, 1),
(1, 4),
(2, 1),
(2, 2),
(2, 4);

-- --------------------------------------------------------

--
-- Structure de la table `quiz_question`
--

DROP TABLE IF EXISTS `quiz_question`;
CREATE TABLE IF NOT EXISTS `quiz_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quiz_id` int(11) NOT NULL,
  `question` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_6033B00B853CD175` (`quiz_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `quiz_question`
--

INSERT INTO `quiz_question` (`id`, `quiz_id`, `question`) VALUES
(1, 1, 'POO c\'est quoi'),
(2, 1, 'Objet c\'est quoi'),
(3, 2, 'What');

-- --------------------------------------------------------

--
-- Structure de la table `quiz_question_answer`
--

DROP TABLE IF EXISTS `quiz_question_answer`;
CREATE TABLE IF NOT EXISTS `quiz_question_answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quiz_question_id` int(11) DEFAULT NULL,
  `answer` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `score` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_E684DF7C3101E51F` (`quiz_question_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `quiz_question_answer`
--

INSERT INTO `quiz_question_answer` (`id`, `quiz_question_id`, `answer`, `score`) VALUES
(1, 1, 'Programmation Orientée Objet', 20),
(2, 1, 'test ', 10),
(3, 2, 'sdfsdfs sarako be', 10),
(4, 2, 'un objet', 15),
(5, 3, 'sdfsdfs sarako be', 10),
(6, 3, 'watsana', 11),
(7, 3, 'variable', 15);

-- --------------------------------------------------------

--
-- Structure de la table `quiz_question_hard_skills`
--

DROP TABLE IF EXISTS `quiz_question_hard_skills`;
CREATE TABLE IF NOT EXISTS `quiz_question_hard_skills` (
  `quiz_question_id` int(11) NOT NULL,
  `hard_skills_id` int(11) NOT NULL,
  PRIMARY KEY (`quiz_question_id`,`hard_skills_id`),
  KEY `IDX_2B2C12AC3101E51F` (`quiz_question_id`),
  KEY `IDX_2B2C12AC44C6C0E1` (`hard_skills_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `quiz_question_hard_skills`
--

INSERT INTO `quiz_question_hard_skills` (`quiz_question_id`, `hard_skills_id`) VALUES
(3, 1),
(3, 2);

-- --------------------------------------------------------

--
-- Structure de la table `quiz_type`
--

DROP TABLE IF EXISTS `quiz_type`;
CREATE TABLE IF NOT EXISTS `quiz_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quiz_type_name` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `quiz_type`
--

INSERT INTO `quiz_type` (`id`, `quiz_type_name`) VALUES
(1, 'QCM'),
(2, 'QE');

-- --------------------------------------------------------

--
-- Structure de la table `role`
--

DROP TABLE IF EXISTS `role`;
CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_label` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `sector`
--

DROP TABLE IF EXISTS `sector`;
CREATE TABLE IF NOT EXISTS `sector` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` int(11) NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `concept_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `sector`
--

INSERT INTO `sector` (`id`, `label`, `code`, `description`, `concept_type`) VALUES
(1, 'Prmaire', 120, 'factor', 'danger');

-- --------------------------------------------------------

--
-- Structure de la table `soft_skills`
--

DROP TABLE IF EXISTS `soft_skills`;
CREATE TABLE IF NOT EXISTS `soft_skills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ss_name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `soft_skills`
--

INSERT INTO `soft_skills` (`id`, `ss_name`) VALUES
(1, 'Rigoureux'),
(2, 'Calme'),
(3, 'Travailleur');

-- --------------------------------------------------------

--
-- Structure de la table `ss_candidat`
--

DROP TABLE IF EXISTS `ss_candidat`;
CREATE TABLE IF NOT EXISTS `ss_candidat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `ss_level` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_B3455E3AA76ED395` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `test`
--

DROP TABLE IF EXISTS `test`;
CREATE TABLE IF NOT EXISTS `test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `city`
--
ALTER TABLE `city`
  ADD CONSTRAINT `FK_2D5B0234F92F3E70` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`);

--
-- Contraintes pour la table `domain`
--
ALTER TABLE `domain`
  ADD CONSTRAINT `FK_A7A91E0BDE95C867` FOREIGN KEY (`sector_id`) REFERENCES `sector` (`id`);

--
-- Contraintes pour la table `hs_candidat`
--
ALTER TABLE `hs_candidat`
  ADD CONSTRAINT `FK_3E921DB544C6C0E1` FOREIGN KEY (`hard_skills_id`) REFERENCES `hard_skills` (`id`),
  ADD CONSTRAINT `FK_3E921DB5D7E8C17D` FOREIGN KEY (`info_candidat_id`) REFERENCES `info_candidat` (`id`);

--
-- Contraintes pour la table `koann_user`
--
ALTER TABLE `koann_user`
  ADD CONSTRAINT `FK_AAF5717C115F0EE5` FOREIGN KEY (`domain_id`) REFERENCES `domain` (`id`),
  ADD CONSTRAINT `FK_AAF5717CED16FA20` FOREIGN KEY (`metier_id`) REFERENCES `metier` (`id`);

--
-- Contraintes pour la table `metier`
--
ALTER TABLE `metier`
  ADD CONSTRAINT `FK_51A00D8C115F0EE5` FOREIGN KEY (`domain_id`) REFERENCES `domain` (`id`);

--
-- Contraintes pour la table `metier_hard_skills`
--
ALTER TABLE `metier_hard_skills`
  ADD CONSTRAINT `FK_B8BA403C44C6C0E1` FOREIGN KEY (`hard_skills_id`) REFERENCES `hard_skills` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_B8BA403CED16FA20` FOREIGN KEY (`metier_id`) REFERENCES `metier` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `quiz`
--
ALTER TABLE `quiz`
  ADD CONSTRAINT `FK_A412FA92D7162133` FOREIGN KEY (`quiz_type_id`) REFERENCES `quiz_type` (`id`);

--
-- Contraintes pour la table `quiz_candidat_answer`
--
ALTER TABLE `quiz_candidat_answer`
  ADD CONSTRAINT `FK_5F120D08853CD175` FOREIGN KEY (`quiz_id`) REFERENCES `quiz` (`id`),
  ADD CONSTRAINT `FK_5F120D088D0EB82` FOREIGN KEY (`candidat_id`) REFERENCES `koann_user` (`id`);

--
-- Contraintes pour la table `quiz_candidat_response`
--
ALTER TABLE `quiz_candidat_response`
  ADD CONSTRAINT `FK_F6E900E83101E51F` FOREIGN KEY (`quiz_question_id`) REFERENCES `quiz_question` (`id`),
  ADD CONSTRAINT `FK_F6E900E8A76ED395` FOREIGN KEY (`user_id`) REFERENCES `koann_user` (`id`),
  ADD CONSTRAINT `FK_F6E900E8D4D53BE0` FOREIGN KEY (`quiz_response_id`) REFERENCES `quiz_question_answer` (`id`);

--
-- Contraintes pour la table `quiz_metier`
--
ALTER TABLE `quiz_metier`
  ADD CONSTRAINT `FK_BCE4FDD5853CD175` FOREIGN KEY (`quiz_id`) REFERENCES `quiz` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_BCE4FDD5ED16FA20` FOREIGN KEY (`metier_id`) REFERENCES `metier` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `quiz_question`
--
ALTER TABLE `quiz_question`
  ADD CONSTRAINT `FK_6033B00B853CD175` FOREIGN KEY (`quiz_id`) REFERENCES `quiz` (`id`);

--
-- Contraintes pour la table `quiz_question_answer`
--
ALTER TABLE `quiz_question_answer`
  ADD CONSTRAINT `FK_E684DF7C3101E51F` FOREIGN KEY (`quiz_question_id`) REFERENCES `quiz_question` (`id`);

--
-- Contraintes pour la table `quiz_question_hard_skills`
--
ALTER TABLE `quiz_question_hard_skills`
  ADD CONSTRAINT `FK_2B2C12AC3101E51F` FOREIGN KEY (`quiz_question_id`) REFERENCES `quiz_question` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_2B2C12AC44C6C0E1` FOREIGN KEY (`hard_skills_id`) REFERENCES `hard_skills` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `ss_candidat`
--
ALTER TABLE `ss_candidat`
  ADD CONSTRAINT `FK_B3455E3AA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
